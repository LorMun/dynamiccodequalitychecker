This project represents an IntelliJ plugin. Goal of this plugin is to provide an all-in-one static code quality tool, used for a large amount of code analyse frameworks acress many different languages.

This plugin uses the warning-ng Jenkins plugin, for parsing the different report formats.
An overview of supported frameworks can be found here: https://github.com/jenkinsci/warnings-ng-plugin/blob/master/SUPPORTED-FORMATS.md

For creating the reports the plugin uses to local commandline. Therefor it is mandatory to have some way to create resultfiles via commandline. The created reports are then parsed and displayed. In the configuration of this plugin, any amount of resultfiles can be added. All confiugurated resultfiles are parsed, sorted and displayed on one single view.

Additionally the plugin can ask the warnings-ng plugin when installed on Jenkins to get the Code Analyses Frameworks of the last build.
