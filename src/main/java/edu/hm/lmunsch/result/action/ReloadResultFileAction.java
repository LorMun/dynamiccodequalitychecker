package edu.hm.lmunsch.result.action;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.wm.ToolWindow;
import com.intellij.openapi.wm.ToolWindowManager;
import com.intellij.ui.content.Content;
import edu.hm.hafner.analysis.FileReaderFactory;
import edu.hm.hafner.analysis.IssueParser;
import edu.hm.hafner.analysis.ParsingException;
import edu.hm.hafner.analysis.ReaderFactory;
import edu.hm.hafner.analysis.Report;
import edu.hm.hafner.analysis.registry.ParserDescriptor;
import edu.hm.hafner.analysis.registry.ParserRegistry;
import edu.hm.lmunsch.result.action.util.CommandResult;
import edu.hm.lmunsch.result.action.util.CommandResultType;
import edu.hm.lmunsch.result.action.util.RemoteReportRetrieveService;
import edu.hm.lmunsch.result.ui.MainResultDisplayPanel;
import edu.hm.lmunsch.settings.configProvider.ConfigProvider;
import edu.hm.lmunsch.settings.configProvider.ParserAndFileLocation;
import edu.hm.lmunsch.settings.configProvider.RemoteReportConfigProvider;
import edu.hm.lmunsch.settings.entity.CommandConfigDialogData;
import edu.hm.lmunsch.settings.entity.RemoteReportConfigDialogData;
import org.jetbrains.annotations.NotNull;

import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class ReloadResultFileAction extends AnAction {

    private final static String ID_TOOLWINDOW = "DynamicCodeQualityChecker";

    @Override
    public void actionPerformed(@NotNull AnActionEvent event) {
        MainResultDisplayPanel mainResultDisplayPanel = panelFor(event.getProject());

        CommandConfigDialogData selectedLocalConfiguration = loadSelectedLocalConfiguration(mainResultDisplayPanel);
        RemoteReportConfigDialogData selectedRemoteConfiguration = loadSelectedRemoteReportConfiguration(mainResultDisplayPanel);

        if (selectedLocalConfiguration == null && selectedRemoteConfiguration == null) {
            createErrorResult(mainResultDisplayPanel, "Please select a configuration");
        } else {
            if (selectedLocalConfiguration != null) {
                handleLocalCommand(selectedLocalConfiguration, mainResultDisplayPanel);
            } else {
                try {
                    handleRemoteConfig(mainResultDisplayPanel, selectedRemoteConfiguration);
                } catch (IllegalArgumentException exception) {
                    createErrorResult(mainResultDisplayPanel, exception.getMessage());
                }
            }
        }
    }

    private void handleRemoteConfig(MainResultDisplayPanel mainResultDisplayPanel, RemoteReportConfigDialogData remoteReportConfigDialogData) {
        Report report = RemoteReportRetrieveService.retrieveRemoteReport(remoteReportConfigDialogData.getUrl());
        Map<String, Report> allGeneratedReports = new HashMap<>();
        allGeneratedReports.put("remote", report);
        mainResultDisplayPanel.updateResultTree(allGeneratedReports);
    }

    private void handleLocalCommand(CommandConfigDialogData selectedLocalConfiguration, MainResultDisplayPanel mainResultDisplayPanel) {
        Map<String, Report> allGeneratedReports = new HashMap<>();

        boolean didErrorOccure = false;

        List<ParserDescriptor> descriptors = getDescriptor(selectedLocalConfiguration);
        for (ParserDescriptor descriptor : descriptors) {
            try {
                Report report = generateReport(selectedLocalConfiguration, descriptor);
                allGeneratedReports.put(descriptor.getName(), report);
            } catch (ParsingException parsingException) {
                createErrorResult(mainResultDisplayPanel, parsingException.getMessage());
                didErrorOccure = true;
            }
        }
        if (!didErrorOccure) {
            mainResultDisplayPanel.updateResultTree(allGeneratedReports);
        }
    }

    private Report generateReport(CommandConfigDialogData selectedConfiguration, ParserDescriptor descriptor) throws ParsingException {
        IssueParser parser = descriptor.createParser();
        Optional<ParserAndFileLocation> parserAndFileLocationByDescriptor = selectedConfiguration.getParserAndFileLocationByDescriptor(descriptor);

        if (parserAndFileLocationByDescriptor.isPresent()) {
            ReaderFactory readerFactory = new FileReaderFactory(Paths.get(parserAndFileLocationByDescriptor.get().getFilelocation()));
            Report report = parser.parse(readerFactory);
            return report;
        } else {
            throw new ParsingException("Please verify your configuration is correct");
        }

    }

    private List<ParserDescriptor> getDescriptor(CommandConfigDialogData selectedConfiguration) {
        List<ParserDescriptor> descriptors = new ArrayList<>();
        // TODO check if this can be done more effectifly
        List<ParserDescriptor> allDescriptors = new ParserRegistry().getAllDescriptors();
        for (ParserAndFileLocation parserAndFileLocation : selectedConfiguration.getParserAndFileLocations()) {
            String parserName = parserAndFileLocation.getParserName();
            ParserDescriptor descriptor = getParserDescriptorForName(allDescriptors, parserName);
            descriptors.add(descriptor);
        }
        return descriptors;
    }

    private ParserDescriptor getParserDescriptorForName(List<ParserDescriptor> allDescriptors, String parserName) {
        for (ParserDescriptor parserDescriptor : allDescriptors) {
            if (parserDescriptor.getName().equals(parserName)) {
                return parserDescriptor;
            }
        }
        return null;
    }

    private void createErrorResult(MainResultDisplayPanel mainResultDisplayPanel, String s) {
        CommandResult errorMessage = new CommandResult(CommandResultType.ERROR, s);
        mainResultDisplayPanel.setErrorMessage(errorMessage);
    }

    private CommandConfigDialogData loadSelectedLocalConfiguration(MainResultDisplayPanel mainResultDisplayPanel) {
        String configName = (String) mainResultDisplayPanel.getRuleDropDownModel().getSelectedItem();

        ConfigProvider configProvider = mainResultDisplayPanel.loadCommandConfigEntries();
        CommandConfigDialogData selectedConfiguration = configProvider.getState().getConfigurationForName(configName);
        return selectedConfiguration;
    }

    private RemoteReportConfigDialogData loadSelectedRemoteReportConfiguration(MainResultDisplayPanel mainResultDisplayPanel) {
        String configName = (String) mainResultDisplayPanel.getRuleDropDownModel().getSelectedItem();

        RemoteReportConfigProvider remoteReportConfigProvider = mainResultDisplayPanel.loadRemoteReportConfigEntries();
        RemoteReportConfigDialogData remoteConfiguration = remoteReportConfigProvider.getState().getConfigurationForName(configName);
        return remoteConfiguration;
    }


    static ToolWindow toolWindow(final Project project) {
        return ToolWindowManager
                .getInstance(project)
                .getToolWindow(ID_TOOLWINDOW);
    }


    public static MainResultDisplayPanel panelFor(final Project project) {
        final ToolWindow toolWindow = toolWindow(project);
        if (toolWindow == null) {
            return null;
        }
        for (Content currentContent : toolWindow.getContentManager().getContents()) {
            if (currentContent.getComponent() instanceof MainResultDisplayPanel) {
                return (MainResultDisplayPanel) currentContent.getComponent();
            }
        }

        return null;
    }

}
