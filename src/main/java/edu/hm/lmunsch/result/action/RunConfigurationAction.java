package edu.hm.lmunsch.result.action;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.wm.ToolWindow;
import com.intellij.openapi.wm.ToolWindowManager;
import com.intellij.ui.content.Content;
import edu.hm.hafner.analysis.FileReaderFactory;
import edu.hm.hafner.analysis.IssueParser;
import edu.hm.hafner.analysis.ParsingException;
import edu.hm.hafner.analysis.ReaderFactory;
import edu.hm.hafner.analysis.Report;
import edu.hm.hafner.analysis.registry.ParserDescriptor;
import edu.hm.hafner.analysis.registry.ParserRegistry;
import edu.hm.lmunsch.result.action.util.CommandLineRunner;
import edu.hm.lmunsch.result.action.util.CommandResult;
import edu.hm.lmunsch.result.action.util.CommandResultType;
import edu.hm.lmunsch.result.action.util.RemoteReportRetrieveService;
import edu.hm.lmunsch.result.ui.MainResultDisplayPanel;
import edu.hm.lmunsch.settings.configProvider.ConfigProvider;
import edu.hm.lmunsch.settings.configProvider.ParserAndFileLocation;
import edu.hm.lmunsch.settings.configProvider.RemoteReportConfigProvider;
import edu.hm.lmunsch.settings.entity.CommandConfigDialogData;
import edu.hm.lmunsch.settings.entity.RemoteReportConfigDialogData;
import org.jetbrains.annotations.NotNull;

import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class RunConfigurationAction extends AnAction {

    private final static String ID_TOOLWINDOW = "DynamicCodeQualityChecker";

    private final static String ACTION_BUTTONS = "actionButtons";

    @Override
    public void actionPerformed(@NotNull AnActionEvent event) {
        MainResultDisplayPanel mainResultDisplayPanel = panelFor(event.getProject());

        CommandConfigDialogData selectedLocalConfiguration = loadSelectedCommandConfiguration(mainResultDisplayPanel);
        RemoteReportConfigDialogData selectedRemoteConfiguration = loadSelectedRemoteReportConfiguration(mainResultDisplayPanel);

        if (selectedLocalConfiguration == null && selectedRemoteConfiguration == null) {
            createErrorResult(mainResultDisplayPanel, "Please select a configuration");
        } else {
            if (selectedLocalConfiguration != null) {
                handleLocalCommandConfiguration(event, mainResultDisplayPanel, selectedLocalConfiguration);
            } else {
                try {
                    handleRemoteConfig(mainResultDisplayPanel, selectedRemoteConfiguration);
                } catch (IllegalArgumentException exception) {
                    createErrorResult(mainResultDisplayPanel, exception.getMessage());
                }
            }
        }
    }

    private void createErrorResult(MainResultDisplayPanel mainResultDisplayPanel, String s) {
        CommandResult errorMessage = new CommandResult(CommandResultType.ERROR, s);
        mainResultDisplayPanel.setErrorMessage(errorMessage);
    }

    private void handleRemoteConfig(MainResultDisplayPanel mainResultDisplayPanel, RemoteReportConfigDialogData remoteReportConfigDialogData) {
        Report report = RemoteReportRetrieveService.retrieveRemoteReport(remoteReportConfigDialogData.getUrl());
        Map<String, Report> allGeneratedReports = new HashMap<>();
        allGeneratedReports.put("remote", report);
        mainResultDisplayPanel.updateResultTree(allGeneratedReports);
    }

    private void handleLocalCommandConfiguration(@NotNull AnActionEvent event, MainResultDisplayPanel mainResultDisplayPanel, CommandConfigDialogData selectedConfiguration) {
        Map<String, Report> allGeneratedReports = new HashMap<>();
        boolean didErrorOccure = false;

        if (selectedConfiguration != null) {
            CommandResult commandResult = runCommandline(event, selectedConfiguration);

            if (commandResult.getResultType() == CommandResultType.ERROR) {
                mainResultDisplayPanel.setErrorMessage(commandResult);
            } else {
                List<ParserDescriptor> descriptors = getDescriptor(selectedConfiguration);
                for (ParserDescriptor descriptor : descriptors) {
                    if (descriptor != null) {
                        try {
                            Report report = generateReport(selectedConfiguration, descriptor);
                            allGeneratedReports.put(descriptor.getName(), report);
                        } catch (ParsingException parsingException) {
                            createErrorResult(mainResultDisplayPanel, parsingException.getMessage());
                            didErrorOccure = true;
                        }
                    }
                }
                if (!didErrorOccure) {
                    mainResultDisplayPanel.updateResultTree(allGeneratedReports);
                }
            }
        }
    }


    private Report generateReport(CommandConfigDialogData selectedConfiguration, ParserDescriptor descriptor) throws ParsingException {
        IssueParser parser = descriptor.createParser();
        Optional<ParserAndFileLocation> parserAndFileLocationByDescriptor = selectedConfiguration.getParserAndFileLocationByDescriptor(descriptor);

        if (parserAndFileLocationByDescriptor.isPresent()) {
            ReaderFactory readerFactory = new FileReaderFactory(Paths.get(parserAndFileLocationByDescriptor.get().getFilelocation()));
            Report report = parser.parse(readerFactory);
            return report;
        } else {
            throw new ParsingException("Please verify your configuration is correct");
        }
    }

    private List<ParserDescriptor> getDescriptor(CommandConfigDialogData selectedConfiguration) {
        List<ParserDescriptor> descriptors = new ArrayList<>();
        List<ParserDescriptor> allDescriptors = new ParserRegistry().getAllDescriptors();
        for (ParserAndFileLocation parserAndFileLocation : selectedConfiguration.getParserAndFileLocations()) {
            String parserName = parserAndFileLocation.getParserName();
            ParserDescriptor descriptor = getParserDescriptorForName(allDescriptors, parserName);
            descriptors.add(descriptor);
        }
        return descriptors;
    }

    private ParserDescriptor getParserDescriptorForName(List<ParserDescriptor> allDescriptors, String parserName) {
        for (ParserDescriptor parserDescriptor : allDescriptors) {
            if (parserDescriptor.getName().equals(parserName)) {
                return parserDescriptor;
            }
        }
        return null;
    }

    private CommandResult runCommandline(@NotNull AnActionEvent event, CommandConfigDialogData selectedConfiguration) {
        CommandLineRunner commandLineRunner = new CommandLineRunner();
        CommandResult commandResult = commandLineRunner.runCommand(selectedConfiguration.getCommand(), event.getProject().getBasePath());
        return commandResult;
    }

    private CommandConfigDialogData loadSelectedCommandConfiguration(MainResultDisplayPanel mainResultDisplayPanel) {
        String configName = (String) mainResultDisplayPanel.getRuleDropDownModel().getSelectedItem();

        ConfigProvider configProvider = mainResultDisplayPanel.loadCommandConfigEntries();
        CommandConfigDialogData localCommandConfiguration = configProvider.getState().getConfigurationForName(configName);
        return localCommandConfiguration;
    }

    private RemoteReportConfigDialogData loadSelectedRemoteReportConfiguration(MainResultDisplayPanel mainResultDisplayPanel) {
        String configName = (String) mainResultDisplayPanel.getRuleDropDownModel().getSelectedItem();

        RemoteReportConfigProvider remoteReportConfigProvider = mainResultDisplayPanel.loadRemoteReportConfigEntries();
        RemoteReportConfigDialogData remoteConfiguration = remoteReportConfigProvider.getState().getConfigurationForName(configName);
        return remoteConfiguration;
    }


    static ToolWindow toolWindow(final Project project) {
        return ToolWindowManager
                .getInstance(project)
                .getToolWindow(ID_TOOLWINDOW);
    }


    public static MainResultDisplayPanel panelFor(final Project project) {
        final ToolWindowManager toolWindowManager = ToolWindowManager.getInstance(project);
        if (toolWindowManager == null) {
            return null;
        }
        final ToolWindow toolWindow = toolWindowManager.getToolWindow(ID_TOOLWINDOW);
        if (toolWindow == null) {
            return null;
        }
        for (Content currentContent : toolWindow.getContentManager().getContents()) {
            if (currentContent.getComponent() instanceof MainResultDisplayPanel) {
                return (MainResultDisplayPanel) currentContent.getComponent();
            }
        }

        return null;
    }

}
