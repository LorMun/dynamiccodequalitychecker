package edu.hm.lmunsch.result.action.jsonEntity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RemoteResponseControlEntity {

    @JsonProperty("_class")
    private String _class;

    @JsonProperty("tools")
    private List<SupportedRemoteParserEntity> supportedRemoteParserEntity;

    @JsonProperty("issues")
    private List<IssueEntity> issues;

    public List<SupportedRemoteParserEntity> getSupportedRemoteParserEntity() {
        return supportedRemoteParserEntity;
    }

    public String get_class() {
        return _class;
    }

    public List<IssueEntity> getIssues() {
        return issues;
    }
}
