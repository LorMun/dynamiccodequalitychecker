package edu.hm.lmunsch.result.action.jsonEntity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SupportedRemoteParserEntity {

    @JsonProperty("id")
    private String id;

    @JsonProperty("latestUrl")
    private String latestUrl;

    @JsonProperty("name")
    private String name;

    @JsonProperty("size")
    private int size;


    public String getId() {
        return id;
    }

    public String getLatestUrl() {
        return latestUrl;
    }

    public String getName() {
        return name;
    }

    public int getSize() {
        return size;
    }
}
