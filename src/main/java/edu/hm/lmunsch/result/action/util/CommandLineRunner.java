package edu.hm.lmunsch.result.action.util;

import org.apache.commons.io.FilenameUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class CommandLineRunner {

    public CommandResult runCommand(String command, String projectPath) {
        boolean isWindows = System.getProperty("os.name")
                .toLowerCase().startsWith("windows");
        CommandResult result;
        try {
            if (isWindows) {
                result = runWindowsCommand(command, projectPath);
            } else {
                result = runLinuxCommand(command, projectPath);
            }
        } catch (IOException | InterruptedException e) {
            result = new CommandResult(CommandResultType.ERROR, "Something went wrong. Please check your configuration");
        }
        return result;
    }

    private CommandResult runWindowsCommand(String command, String projectPath) throws IOException, InterruptedException {
        ProcessBuilder builder = new ProcessBuilder(
                "cmd.exe", "/c", "cd \"" + FilenameUtils.separatorsToSystem(projectPath) + "\" && " + command);

        Process process = builder.start();
        process.waitFor();

        return getCommandResult(process);
    }

    private CommandResult runLinuxCommand(String command, String projectPath) throws IOException, InterruptedException {
        ProcessBuilder builder = new ProcessBuilder(
                "/bin/bash", "-ic", "cd " + FilenameUtils.separatorsToSystem(projectPath) + " && " + command);
        Process process = builder.start();
        process.waitFor();

        return getCommandResult(process);
    }


    private CommandResult getCommandResult(Process process) {
        CommandResultType commandResultType = checkCommandSuccessfull(process);
        BufferedReader bufferedReader;
        if (commandResultType.equals(CommandResultType.SUCCESFULL)) {
            bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
        } else {
            bufferedReader = new BufferedReader(new InputStreamReader(process.getErrorStream()));
        }
        String line = "";
        String resultString = "";
        while (true) {
            try {
                line = bufferedReader.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            resultString += line;
            if (line == null) {
                break;
            }
        }
        return new CommandResult(commandResultType, resultString);
    }

    private CommandResultType checkCommandSuccessfull(Process process) {
        boolean result = false;
        try {
            result = process.getErrorStream().available() == 0;
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (result) {
            return CommandResultType.SUCCESFULL;
        } else {
            return CommandResultType.ERROR;
        }
    }

}
