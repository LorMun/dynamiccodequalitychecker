package edu.hm.lmunsch.result.action.util;

public class CommandResult {

    private final CommandResultType resultType;
    private final String returnString;

    public CommandResult(CommandResultType resultType, String returnString) {
        this.resultType = resultType;
        this.returnString = returnString;
    }

    public CommandResultType getResultType() {
        return resultType;
    }

    public String getReturnString() {
        return returnString;
    }
}
