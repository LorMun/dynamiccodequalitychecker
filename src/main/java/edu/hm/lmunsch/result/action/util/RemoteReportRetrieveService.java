package edu.hm.lmunsch.result.action.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.hm.hafner.analysis.IssueBuilder;
import edu.hm.hafner.analysis.Report;
import edu.hm.lmunsch.result.action.jsonEntity.IssueEntity;
import edu.hm.lmunsch.result.action.jsonEntity.RemoteResponseControlEntity;
import edu.hm.lmunsch.result.action.jsonEntity.SupportedRemoteParserEntity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class RemoteReportRetrieveService {

    public static Report retrieveRemoteReport(String urlString) throws IllegalArgumentException  {
        if (!urlString.toLowerCase().contains("http")) {
            throw new IllegalArgumentException("The remote address must start with http(s)!");
        }
        Report report = new Report();

        try {
            ObjectMapper mapper = new ObjectMapper();
            urlString = prepareUrlString(urlString);
            RemoteResponseControlEntity supportedParsers = getSupportedParsers(mapper, urlString);
            List<IssueEntity> remoteIssues = getRemoteIssues(mapper, supportedParsers);
            remoteResponseToReport(report, remoteIssues);
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
        return report;
    }

    private static void remoteResponseToReport(Report report, List<IssueEntity> remoteIssues) {
        for (IssueEntity issueEntity : remoteIssues) {
            IssueBuilder builder = new IssueBuilder();
            builder.setMessage(issueEntity.getMessage());
            builder.setLineStart(issueEntity.getLineStart());
            builder.setLineEnd(issueEntity.getLineEnd());
            builder.setColumnStart(issueEntity.getColumnStart());
            builder.setColumnEnd(issueEntity.getColumnEnd());
            builder.setPathName(issueEntity.getFileName());
            builder.setFileName(issueEntity.getBaseName());

            report.add(builder.build());
        }
    }

    private static String prepareUrlString(String urlString) {
        if (urlString.endsWith("/")) {
            urlString = urlString.substring(0, urlString.length() - 1);
        }

        return urlString;
    }

    private static RemoteResponseControlEntity getSupportedParsers(ObjectMapper mapper, String urlString) throws IOException {
        URL supportedParsersUrl = new URL(urlString + "/lastBuild/warnings-ng/api/json");
        HttpURLConnection con = (HttpURLConnection) supportedParsersUrl.openConnection();
        String supportedParserJson = getContentOfResponse(con);

        RemoteResponseControlEntity supportedParsers = mapper.readValue(supportedParserJson, RemoteResponseControlEntity.class);
        con.disconnect();
        return supportedParsers;
    }

    private static List<IssueEntity> getRemoteIssues(ObjectMapper mapper, RemoteResponseControlEntity supportedParsers) throws IOException {
        HttpURLConnection con;
        for (SupportedRemoteParserEntity supportedRemoteParserEntity : supportedParsers.getSupportedRemoteParserEntity()) {
            URL parserUrl = new URL(supportedRemoteParserEntity.getLatestUrl() + "/all/api/json");
            con = (HttpURLConnection) parserUrl.openConnection();
            String issuesJson = getContentOfResponse(con);
            RemoteResponseControlEntity issueEntities = mapper.readValue(issuesJson, RemoteResponseControlEntity.class);
            con.disconnect();

            return issueEntities.getIssues();

        }
        return new ArrayList<>();
    }


    private static String getContentOfResponse(HttpURLConnection con) {
        StringBuilder result = new StringBuilder();

        if (con != null) {
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String input;

                while ((input = br.readLine()) != null) {
                    result.append(input);
                }
                br.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result.toString();
    }
}
