package edu.hm.lmunsch.result.ui;

import com.intellij.openapi.actionSystem.ActionGroup;
import com.intellij.openapi.actionSystem.ActionManager;
import com.intellij.openapi.actionSystem.ActionToolbar;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.application.ModalityState;
import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.editor.LogicalPosition;
import com.intellij.openapi.editor.ScrollType;
import com.intellij.openapi.fileEditor.FileEditor;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.fileEditor.TextEditor;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.ComboBox;
import com.intellij.openapi.vfs.VfsUtil;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.ui.components.JBScrollPane;
import com.intellij.ui.treeStructure.Tree;
import edu.hm.hafner.analysis.Report;
import edu.hm.lmunsch.result.action.util.CommandResult;
import edu.hm.lmunsch.result.ui.resultTree.CustomResultTreeCellRenderer;
import edu.hm.lmunsch.result.ui.resultTree.ResultTreeModel;
import edu.hm.lmunsch.result.ui.resultTree.treeNode.TreeNode;
import edu.hm.lmunsch.settings.configProvider.ConfigListener;
import edu.hm.lmunsch.settings.configProvider.ConfigProvider;
import edu.hm.lmunsch.settings.configProvider.RemoteReportConfigProvider;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.nio.file.Paths;
import java.util.Map;


public class MainResultDisplayPanel extends JPanel implements ConfigListener {

    private final Project project;

    private final static String ID_TOOLWINDOW = "DynamicCodeQualityChecker";
    private final static String ACTION_BUTTONS = "actionButtons";
    private final static String DEFAULT_DROPDOWN_ELEMENT = "Select Configuration";

    private JTree resultsTree;
    private JToolBar configPickerToolBar;
    private ResultTreeModel treeModel;
    private final ComboBox ruleDropdown = new ComboBox();
    private final DefaultComboBoxModel ruleDropDownModel = new DefaultComboBoxModel();


    public MainResultDisplayPanel(final Project project) {
        super(new BorderLayout());
        this.project = project;

        final Box toolBarBox = createActionBox();
        add(toolBarBox, BorderLayout.WEST);
        add(createMainPanel(), BorderLayout.CENTER);

        expandTree();
    }

    @NotNull
    private Box createActionBox() {
        final ActionGroup mainActionGroup = (ActionGroup)
                ActionManager.getInstance().getAction(ACTION_BUTTONS);
        final ActionToolbar mainToolbar = ActionManager.getInstance().createActionToolbar(
                ID_TOOLWINDOW, mainActionGroup, false);

        final Box toolBarBox = Box.createHorizontalBox();
        toolBarBox.add(mainToolbar.getComponent());
        return toolBarBox;
    }


    private JPanel createMainPanel() {

        createResultPanel();

        createDropdownPanel();

        final JPanel toolPanel = new JPanel(new BorderLayout());
        toolPanel.add(new JBScrollPane(resultsTree), BorderLayout.CENTER);
        toolPanel.add(configPickerToolBar, BorderLayout.NORTH);

        return toolPanel;
    }

    private void createResultPanel() {
        treeModel = new ResultTreeModel();

        resultsTree = new Tree(treeModel);
        resultsTree.setRootVisible(false);
        resultsTree.setCellRenderer(new CustomResultTreeCellRenderer());


        final TreeSelectionListener treeSelectionListener = new ResultDisplaySelectionListener();
        resultsTree.addTreeSelectionListener(treeSelectionListener);
        final MouseListener treeMouseListener = new ResultDisplayMouseListener();
        resultsTree.addMouseListener(treeMouseListener);
    }

    private void createDropdownPanel() {
        configurationChanged();
        loadCommandConfigEntries().addConfigurationListener(this);
        loadRemoteReportConfigEntries().addConfigurationListener(this);

        ruleDropdown.setModel(ruleDropDownModel);
        final int preferredHeight = ruleDropdown.getPreferredSize().height;
        ruleDropdown.setPreferredSize(new Dimension(250, preferredHeight));
        ruleDropdown.setMaximumSize(new Dimension(350, preferredHeight));

        configPickerToolBar = new JToolBar(JToolBar.HORIZONTAL);
        configPickerToolBar.add(Box.createHorizontalStrut(4));
        configPickerToolBar.add(new JLabel("Config:"));
        configPickerToolBar.add(Box.createHorizontalStrut(4));
        configPickerToolBar.add(ruleDropdown);
        configPickerToolBar.setFloatable(false);
        configPickerToolBar.setOpaque(false);
        configPickerToolBar.setBorder(null);
    }


    public void updateResultTree(Map<String, Report> report) {
        treeModel.setModel(report);
        resultsTree.setRootVisible(false);

        invalidate();
        repaint();
        expandTree();
    }

    @Override
    public void configurationChanged() {
        ruleDropDownModel.removeAllElements();

        ruleDropDownModel.addElement(DEFAULT_DROPDOWN_ELEMENT);
        if (loadCommandConfigEntries().getState().getConfiguration() != null) {
            loadCommandConfigEntries().getState().getConfiguration().forEach((s, configurationDialogData) -> ruleDropDownModel.addElement(s));
        }
        if (loadRemoteReportConfigEntries().getState().getConfiguration() != null) {
            loadRemoteReportConfigEntries().getState().getConfiguration().forEach((s, configurationDialogData) -> ruleDropDownModel.addElement(s));
        }
        ruleDropDownModel.setSelectedItem(DEFAULT_DROPDOWN_ELEMENT);
    }

    public ConfigProvider loadCommandConfigEntries() {
        return ServiceManager.getService(ConfigProvider.class);
    }

    public RemoteReportConfigProvider loadRemoteReportConfigEntries() {
        return ServiceManager.getService(RemoteReportConfigProvider.class);
    }

    public DefaultComboBoxModel getRuleDropDownModel() {
        return ruleDropDownModel;
    }

    public void setErrorMessage(CommandResult commandResult) {
        treeModel.setModel(commandResult);
        resultsTree.setRootVisible(false);

        invalidate();
        repaint();
    }

    public void expandTree() {
        DefaultMutableTreeNode rootNode = treeModel.getRootNode();
        if (rootNode.getDepth() > 0) {
            for (int i = 0; i < treeModel.getRootNode().getChildCount(); i++) {
                TreePath leaf = new TreePath(treeModel.getPathToRoot(treeModel.getRootNode().getChildAt(i)));
                resultsTree.expandPath(leaf);
            }
        }
    }

    private void openFileInEditor(final TreePath treePath) {
        final DefaultMutableTreeNode treeNode = (DefaultMutableTreeNode) treePath.getLastPathComponent();
        if (treeNode == null || !(treeNode.getUserObject() instanceof TreeNode)) {
            return;
        }

        final TreeNode nodeInfo = (TreeNode) treeNode.getUserObject();
        if (nodeInfo.getFile() != null || nodeInfo.getLine() != null || nodeInfo.getPosition() != null) {

            final VirtualFile virtualFile = VfsUtil.findFile(Paths.get(nodeInfo.getFile()), true);
            if (virtualFile == null || !virtualFile.exists()) {
                return;
            }

            final FileEditorManager fileEditorManager = FileEditorManager.getInstance(project);
            ApplicationManager.getApplication().invokeLater(() -> {
                final FileEditor[] editor = fileEditorManager.openFile(
                        virtualFile, true);

                if (editor.length > 0 && editor[0] instanceof TextEditor) {
                    final LogicalPosition problemPos = new LogicalPosition(
                            Math.max(nodeInfo.getLine() - 1, 0), Math.max(nodeInfo.getPosition(), 0));

                    final Editor textEditor = ((TextEditor) editor[0]).getEditor();

                    textEditor.getCaretModel().moveToLogicalPosition(problemPos);
                    textEditor.getScrollingModel().scrollToCaret(ScrollType.CENTER);

                }
            }, ModalityState.NON_MODAL);
        }
    }


    protected class ResultDisplayMouseListener extends MouseAdapter {

        @Override
        public void mouseClicked(final MouseEvent event) {
            if (event.getClickCount() < 2) {
                return;
            }

            final TreePath treePath = resultsTree.getPathForLocation(
                    event.getX(), event.getY());

            if (treePath != null) {
                openFileInEditor(treePath);
            }
        }

    }

    protected class ResultDisplaySelectionListener implements TreeSelectionListener {

        @Override
        public void valueChanged(final TreeSelectionEvent e) {
            if (e.getPath() != null) {
                openFileInEditor(e.getPath());
            }
        }

    }
}


