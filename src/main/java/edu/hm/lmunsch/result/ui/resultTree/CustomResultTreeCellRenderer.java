package edu.hm.lmunsch.result.ui.resultTree;

import edu.hm.lmunsch.result.ui.resultTree.treeNode.TreeNode;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeCellRenderer;
import java.awt.*;

public class CustomResultTreeCellRenderer extends JLabel implements TreeCellRenderer {

    public CustomResultTreeCellRenderer() {
        super();
        setOpaque(false);
    }

    @Override
    public Component getTreeCellRendererComponent(final JTree tree,
                                                  final Object value,
                                                  final boolean selected,
                                                  final boolean expanded,
                                                  final boolean leaf,
                                                  final int row,
                                                  final boolean hasFocus) {
        final DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
        if (node != null) {
            final Object userObject = node.getUserObject();
            if (userObject instanceof TreeNode) {
                final TreeNode treeNode
                        = (TreeNode) userObject;

                setIcon(treeNode.getIcon());
                setText(treeNode.getText());

                //used for testing. Doesn't appear at the frontend.
                setName("test");

                validate();
            } else {
                setIcon(null);

                //used for testing. Doesn't appear at the frontend.
                setName("test-for-null");
            }


        }
        return this;
    }

}
