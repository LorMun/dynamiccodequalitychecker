package edu.hm.lmunsch.result.ui.resultTree;

import edu.hm.hafner.analysis.Issue;
import edu.hm.hafner.analysis.Report;
import edu.hm.hafner.analysis.Severity;
import edu.hm.lmunsch.result.action.util.CommandResult;
import edu.hm.lmunsch.result.ui.resultTree.treeNode.ResultTreeNodeLevel;
import edu.hm.lmunsch.result.ui.resultTree.treeNode.TreeNode;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ResultTreeModel extends DefaultTreeModel {

    private final DefaultMutableTreeNode rootNode;


    public ResultTreeModel() {
        super(new DefaultMutableTreeNode());

        rootNode = new DefaultMutableTreeNode();
        ((DefaultMutableTreeNode) getRoot()).add(rootNode);

    }

    public void resetTree() {
        rootNode.removeAllChildren();
    }

    public void setModel(Map<String, Report> report) {
        Map<String, List<Issue>> resultMap = transformReportToIssueMap(report);
        resetTree();
        rootNode.setUserObject(new TreeNode("Scan results - " + getTotalAmountOfIssues(resultMap) +" Issues in "  + resultMap.keySet().size() + " Files", null,null, null, null, ResultTreeNodeLevel.root));

        Set<String> keys = resultMap.keySet();

        for (String key : keys) {
            final DefaultMutableTreeNode fileNode = new DefaultMutableTreeNode();
            fileNode.setUserObject(new TreeNode(getFileNameFromAbsolutFilePath(key), null,null, null, getHighestSeverityForFile(key, resultMap), ResultTreeNodeLevel.branch));

            List<Issue> issues = resultMap.get(key);
            for (Issue issue : issues) {
                String result = issue.getMessage() + "(" + issue.getLineStart() + ", " + issue.getColumnStart() + ")";
                final DefaultMutableTreeNode issueNode = new DefaultMutableTreeNode();
                issueNode.setUserObject(new TreeNode(result, key, issue.getLineStart(), issue.getColumnStart(), issue.getSeverity(), ResultTreeNodeLevel.leaf));

                fileNode.add(issueNode);
            }

            rootNode.add(fileNode);
        }
        this.reload();
    }

    private int getTotalAmountOfIssues(Map<String, List<Issue>> resultMap) {
        int total = 0;
        for (String file : resultMap.keySet()) {
            List<Issue> issues = resultMap.get(file);
            total += issues.size();
        }
        return total;
    }

    public void setModel(CommandResult commandResult) {
        resetTree();
        rootNode.setUserObject(new TreeNode("Plugin Error - " + commandResult.getReturnString(), null,null, null, null, ResultTreeNodeLevel.error));
        this.reload();

    }

    private Severity getHighestSeverityForFile(String key, Map<String, List<Issue>> resultMap) {

        for (Issue issue : resultMap.get(key)) {
            if (Severity.ERROR.equals(issue.getSeverity())) {
                return Severity.ERROR;
            }
        }
        return Severity.WARNING_NORMAL;
    }


    private Map<String, List<Issue>> transformReportToIssueMap(Map<String, Report> reportMap) {
        Map<String, List<Issue>> resultMap = new HashMap<>();
        for (String key : reportMap.keySet()) {
            int i = 0;
            Report report = reportMap.get(key);
            while (i < report.getSize()) {
                Issue issueToCheck = report.get(i);

                if (resultMap.containsKey(issueToCheck.getAbsolutePath())) {
                    List<Issue> tempList = resultMap.get(issueToCheck.getAbsolutePath());
                    tempList.add(issueToCheck);
                    resultMap.put(issueToCheck.getAbsolutePath(), tempList);
                } else {
                    List<Issue> issues = new ArrayList<>();
                    issues.add(issueToCheck);
                    resultMap.put(issueToCheck.getAbsolutePath(), issues);
                }
                i++;
            }
        }
        return resultMap;
    }

    private String getFileNameFromAbsolutFilePath(String filepath) {
        File file = new File(filepath);
        return file.getName();
    }


    public DefaultMutableTreeNode getRootNode() {
        return rootNode;
    }
}
