package edu.hm.lmunsch.result.ui.resultTree.treeNode;

public enum ResultTreeNodeLevel {

    root, branch, leaf, error

}
