package edu.hm.lmunsch.result.ui.resultTree.treeNode;

import com.intellij.icons.AllIcons;
import edu.hm.hafner.analysis.Severity;

import javax.swing.*;

public class TreeNode {

    private final String text;
    private final String file;
    private final Integer line;
    private final Integer position;
    private final Severity severity;
    private final ResultTreeNodeLevel resultTreeNodeLevel;
    private Icon icon;

    public TreeNode(String text, String file, Integer line, Integer position, Severity severity, ResultTreeNodeLevel resultTreeNodeLevel) {
        this.text = text;
        this.file = file;
        this.line = line;
        this.position = position;
        this.severity = severity;
        this.resultTreeNodeLevel = resultTreeNodeLevel;
        setIcon();
    }



    private void setIcon() {
        switch (resultTreeNodeLevel) {
            case root:
                this.icon = AllIcons.FileTypes.Java;
                break;
            case leaf:
            case branch:
                if (Severity.ERROR.equals(severity)) {
                    this.icon = AllIcons.General.BalloonError;
                } else {
                    this.icon = AllIcons.General.BalloonWarning;
                }
                break;
            case error:
            default:
                this.icon = null;
        }
    }

    public String getText() {
        return text;
    }

    public Icon getIcon() {
        return icon;
    }

    public Integer getLine() {
        return line;
    }

    public Integer getPosition() {
        return position;
    }

    public String getFile() {
        return file;
    }
}
