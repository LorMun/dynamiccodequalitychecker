package edu.hm.lmunsch.settings.action;

import com.intellij.ui.AnActionButton;
import com.intellij.ui.AnActionButtonRunnable;

import javax.swing.*;

public abstract class SettingsAction extends AbstractAction implements AnActionButtonRunnable {

    @Override
    public void run(final AnActionButton anActionButton) {
        actionPerformed(null);
    }

}
