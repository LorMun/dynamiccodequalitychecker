package edu.hm.lmunsch.settings.action.localCommandActions;

import edu.hm.lmunsch.settings.ui.dialog.CreateCommandDialog;
import edu.hm.lmunsch.settings.ui.dialog.CommandDialogInputPanel;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class AddDynmicDialogPartAction extends AbstractAction {

    private final CommandDialogInputPanel commandDialogInputPanel;
    private final CreateCommandDialog createCommandDialog;

    public AddDynmicDialogPartAction(final CommandDialogInputPanel commandDialogInputPanel, CreateCommandDialog createCommandDialog) {
        this.commandDialogInputPanel = commandDialogInputPanel;
        this.createCommandDialog = createCommandDialog;
    }

    @Override
    public void actionPerformed(final ActionEvent e) {
        commandDialogInputPanel.addDynamicDialogPart();
        createCommandDialog.increaseHeigth();
        commandDialogInputPanel.reloadDialog();
    }
}
