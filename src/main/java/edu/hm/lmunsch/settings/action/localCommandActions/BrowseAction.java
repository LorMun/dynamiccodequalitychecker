package edu.hm.lmunsch.settings.action.localCommandActions;

import com.intellij.openapi.fileChooser.FileChooser;
import com.intellij.openapi.fileChooser.FileChooserDescriptor;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.project.ProjectUtil;
import com.intellij.openapi.vfs.LocalFileSystem;
import com.intellij.openapi.vfs.VfsUtilCore;
import com.intellij.openapi.vfs.VirtualFile;
import edu.hm.lmunsch.settings.ui.dialog.CommandDialogInputPanel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.io.File;
import java.nio.file.Paths;

public class BrowseAction extends AbstractAction {

    private final Project project;
    private final CommandDialogInputPanel commandDialogInputPanel;
    private final JTextField textfieldForFilepath;

    public BrowseAction(final Project project, final CommandDialogInputPanel commandDialogInputPanel, JTextField textfieldForFilepath) {
        this.project = project;
        this.commandDialogInputPanel = commandDialogInputPanel;
        this.textfieldForFilepath = textfieldForFilepath;
    }

    @Override
    public void actionPerformed(final ActionEvent e) {
        final VirtualFile toSelect;
        final String configFilePath = fileLocation();
        if (configFilePath != null) {
            toSelect = LocalFileSystem.getInstance().findFileByPath(configFilePath);
        } else {
            toSelect = ProjectUtil.guessProjectDir(project);
        }
        final FileChooserDescriptor descriptor = new FileChooserDescriptor(true, false, false, false, false, false);
        final VirtualFile chosen = FileChooser.chooseFile(descriptor, commandDialogInputPanel, project, toSelect);
        if (chosen != null) {
            final File newConfigFile = VfsUtilCore.virtualToIoFile(chosen);
            textfieldForFilepath.setText(newConfigFile.getAbsolutePath());
        }
    }


    private String fileLocation() {
        final String filename = commandDialogInputPanel.getFileLocationField().getText().trim();

        if (new File(filename).exists()) {
            return filename;
        }

        final File projectRelativePath = projectRelativeFileOf(filename);
        if (projectRelativePath.exists()) {
            return projectRelativePath.getAbsolutePath();
        }

        return filename;
    }

    private File projectRelativeFileOf(final String filename) {
        return Paths.get(new File(project.getBasePath(), filename).getAbsolutePath())
                .normalize()
                .toAbsolutePath()
                .toFile();
    }
}
