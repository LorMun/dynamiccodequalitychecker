package edu.hm.lmunsch.settings.action.localCommandActions;

import com.intellij.openapi.project.Project;
import edu.hm.lmunsch.settings.ui.dialog.CreateCommandDialog;
import edu.hm.lmunsch.settings.ui.model.CommandConfigTableModel;
import edu.hm.lmunsch.settings.action.SettingsAction;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class CreateNewConfigEntryAction extends SettingsAction {

    Project project;
    CommandConfigTableModel commandConfigTableModel;

    public CreateNewConfigEntryAction(final Project project, CommandConfigTableModel commandConfigTableModel) {
        this.project = project;
        this.commandConfigTableModel = commandConfigTableModel;

        putValue(Action.NAME, "Add");
        putValue(Action.SHORT_DESCRIPTION, "Add new Command");
        putValue(Action.LONG_DESCRIPTION, "Add new Command");
    }

    @Override
    public void actionPerformed(final ActionEvent e) {
        final CreateCommandDialog createCommandDialog = new CreateCommandDialog(project, commandConfigTableModel, false, -1);
        createCommandDialog.setVisible(true);
    }
}
