package edu.hm.lmunsch.settings.action.localCommandActions;

import com.intellij.openapi.project.Project;
import com.intellij.ui.table.JBTable;
import edu.hm.lmunsch.settings.entity.CommandConfigDialogData;
import edu.hm.lmunsch.settings.ui.dialog.CreateCommandDialog;
import edu.hm.lmunsch.settings.ui.model.CommandConfigTableModel;
import edu.hm.lmunsch.settings.action.SettingsAction;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class EditExistingConfigEntryAction extends SettingsAction {

    Project project;
    CommandConfigTableModel commandConfigTableModel;
    JBTable commandTable;

    public EditExistingConfigEntryAction(final Project project, CommandConfigTableModel commandConfigTableModel, final JBTable commandTable) {
        this.project = project;
        this.commandConfigTableModel = commandConfigTableModel;
        this.commandTable = commandTable;

        putValue(Action.NAME, "Edit");
        putValue(Action.SHORT_DESCRIPTION, "Edit existing Config");
        putValue(Action.LONG_DESCRIPTION, "Edit existing Config");
    }

    @Override
    public void actionPerformed(final ActionEvent e) {
        int selectedRow = commandTable.getSelectedRow();
        CommandConfigDialogData commandConfigDialogData = commandConfigTableModel.getConfigurationDataAtRow(selectedRow);

        final CreateCommandDialog createCommandDialog = new CreateCommandDialog(project, commandConfigTableModel, true, selectedRow);
        createCommandDialog.prefilValues(commandConfigDialogData);
        createCommandDialog.setVisible(true);
    }
}
