package edu.hm.lmunsch.settings.action.localCommandActions;

import edu.hm.lmunsch.settings.checker.localCommand.LocalCommandCheckerFactory;
import edu.hm.lmunsch.settings.entity.CommandConfigDialogData;
import edu.hm.lmunsch.settings.ui.dialog.CommandDialogInputPanel;
import edu.hm.lmunsch.settings.ui.dialog.CreateCommandDialog;
import edu.hm.lmunsch.settings.ui.model.CommandConfigTableModel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.util.List;

public class FinishLocalCommandCreateWorkflowAction extends AbstractAction {

    private final CreateCommandDialog createCommandDialog;
    private final CommandDialogInputPanel commandDialogInputPanel;
    private final CommandConfigTableModel commandConfigTableModel;
    private final boolean isEdit;
    private final int changedRow;

    public FinishLocalCommandCreateWorkflowAction(final CreateCommandDialog createCommandDialog, final CommandConfigTableModel commandConfigTableModel, final CommandDialogInputPanel commandDialogInputPanel, boolean isEdit, int changedRow) {
        this.createCommandDialog = createCommandDialog;
        this.commandDialogInputPanel = commandDialogInputPanel;
        this.commandConfigTableModel = commandConfigTableModel;
        this.isEdit = isEdit;
        this.changedRow = changedRow;
    }

    @Override
    public void actionPerformed(final ActionEvent e) {
        CommandConfigDialogData configuration = this.commandDialogInputPanel.getConfiguration();

        LocalCommandCheckerFactory localCommandCheckerFactory = new LocalCommandCheckerFactory(configuration, commandConfigTableModel, isEdit);
        List<String> allChecks = localCommandCheckerFactory.performAllChecks();

        if (allChecks.isEmpty()) {
            if (isEdit) {
                commandConfigTableModel.removeConfigurationDataOnRow(changedRow);
            }
            commandConfigTableModel.addRow(configuration);
            createCommandDialog.setVisible(false);
        } else {
            String collect = String.join("\n", allChecks);
            JOptionPane.showMessageDialog(new JFrame(), collect, "Configuration Error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }
}
