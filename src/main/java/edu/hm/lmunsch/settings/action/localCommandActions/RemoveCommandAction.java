package edu.hm.lmunsch.settings.action.localCommandActions;

import com.intellij.openapi.project.Project;
import com.intellij.ui.table.JBTable;
import edu.hm.lmunsch.settings.ui.model.CommandConfigTableModel;
import edu.hm.lmunsch.settings.action.SettingsAction;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class RemoveCommandAction extends SettingsAction {

    Project project;
    CommandConfigTableModel commandConfigTableModel;
    JBTable commandTable;

    public RemoveCommandAction(final Project project, CommandConfigTableModel commandConfigTableModel, final JBTable commandTable) {
        this.project = project;
        this.commandConfigTableModel = commandConfigTableModel;
        this.commandTable = commandTable;

        putValue(Action.NAME, "Remove Config");
        putValue(Action.SHORT_DESCRIPTION, "Remove the selected Configuration");
        putValue(Action.LONG_DESCRIPTION, "Remove the selected Configuration");
    }

    @Override
    public void actionPerformed(final ActionEvent e) {
        int selectedRow = commandTable.getSelectedRow();

        commandConfigTableModel.removeConfigurationDataOnRow(selectedRow);
    }
}
