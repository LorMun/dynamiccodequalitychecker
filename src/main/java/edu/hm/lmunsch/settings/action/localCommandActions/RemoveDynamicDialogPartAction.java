package edu.hm.lmunsch.settings.action.localCommandActions;

import edu.hm.lmunsch.settings.ui.dialog.CreateCommandDialog;
import edu.hm.lmunsch.settings.ui.dialog.CommandDialogInputPanel;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class RemoveDynamicDialogPartAction extends AbstractAction {

    private final CommandDialogInputPanel commandDialogInputPanel;
    private final CreateCommandDialog createCommandDialog;
    private final String deleteButtonName;

    public RemoveDynamicDialogPartAction(CommandDialogInputPanel commandDialogInputPanel, CreateCommandDialog createCommandDialog, String deleteButtonName) {
        this.commandDialogInputPanel = commandDialogInputPanel;
        this.createCommandDialog = createCommandDialog;
        this.deleteButtonName = deleteButtonName;
    }


    @Override
    public void actionPerformed(final ActionEvent e) {
        commandDialogInputPanel.removeDynamicDialogPart(deleteButtonName);
        createCommandDialog.decreaseHeigth();
        commandDialogInputPanel.reloadDialog();
    }
}
