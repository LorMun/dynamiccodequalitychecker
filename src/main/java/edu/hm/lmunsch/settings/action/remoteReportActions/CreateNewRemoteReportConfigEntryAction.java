package edu.hm.lmunsch.settings.action.remoteReportActions;

import com.intellij.openapi.project.Project;
import edu.hm.lmunsch.settings.ui.dialog.CreateRemoteReportConfigDialog;
import edu.hm.lmunsch.settings.ui.model.RemoteReportTableModel;
import edu.hm.lmunsch.settings.action.SettingsAction;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class CreateNewRemoteReportConfigEntryAction extends SettingsAction {

    Project project;
    RemoteReportTableModel remoteReportTableModel;

    public CreateNewRemoteReportConfigEntryAction(final Project project, RemoteReportTableModel remoteReportTableModel) {
        this.project = project;
        this.remoteReportTableModel = remoteReportTableModel;

        putValue(Action.NAME, "Add");
        putValue(Action.SHORT_DESCRIPTION, "Add new Command");
        putValue(Action.LONG_DESCRIPTION, "Add new Command");
    }

    @Override
    public void actionPerformed(final ActionEvent e) {
        final CreateRemoteReportConfigDialog createCommandDialog = new CreateRemoteReportConfigDialog(project, remoteReportTableModel, false, -1);
        createCommandDialog.setVisible(true);
    }
}
