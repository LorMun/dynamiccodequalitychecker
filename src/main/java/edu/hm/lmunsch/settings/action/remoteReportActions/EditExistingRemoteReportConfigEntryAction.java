package edu.hm.lmunsch.settings.action.remoteReportActions;

import com.intellij.openapi.project.Project;
import com.intellij.ui.table.JBTable;
import edu.hm.lmunsch.settings.ui.dialog.CreateRemoteReportConfigDialog;
import edu.hm.lmunsch.settings.entity.RemoteReportConfigDialogData;
import edu.hm.lmunsch.settings.ui.model.RemoteReportTableModel;
import edu.hm.lmunsch.settings.action.SettingsAction;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class EditExistingRemoteReportConfigEntryAction extends SettingsAction {

    Project project;
    RemoteReportTableModel commandConfigTableModel;
    JBTable commandTable;

    public EditExistingRemoteReportConfigEntryAction(final Project project, RemoteReportTableModel commandConfigTableModel, final JBTable commandTable) {
        this.project = project;
        this.commandConfigTableModel = commandConfigTableModel;
        this.commandTable = commandTable;

        putValue(Action.NAME, "Edit");
        putValue(Action.SHORT_DESCRIPTION, "Edit existing Config");
        putValue(Action.LONG_DESCRIPTION, "Edit existing Config");
    }

    @Override
    public void actionPerformed(final ActionEvent e) {
        int selectedRow = commandTable.getSelectedRow();
        RemoteReportConfigDialogData commandConfigDialogData = commandConfigTableModel.getConfigurationDataAtRow(selectedRow);

        final CreateRemoteReportConfigDialog createCommandDialog = new CreateRemoteReportConfigDialog(project, commandConfigTableModel, true, selectedRow);
        createCommandDialog.prefilValues(commandConfigDialogData);
        createCommandDialog.setVisible(true);
    }
}
