package edu.hm.lmunsch.settings.action.remoteReportActions;

import edu.hm.lmunsch.settings.checker.remoteResult.RemoteResultCheckerFactory;
import edu.hm.lmunsch.settings.entity.RemoteReportConfigDialogData;
import edu.hm.lmunsch.settings.ui.dialog.CreateRemoteReportConfigDialog;
import edu.hm.lmunsch.settings.ui.dialog.RemoteReportDialogInputPanel;
import edu.hm.lmunsch.settings.ui.model.RemoteReportTableModel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.util.List;

public class FinishRemoteReportCreateWorkflowAction extends AbstractAction {

    private final CreateRemoteReportConfigDialog createRemoteReportConfigDialog;
    private final RemoteReportDialogInputPanel remoteReportDialogInputPanel;
    private final RemoteReportTableModel remoteReportTableModel;
    private final boolean isEdit;
    private final int changedRow;

    public FinishRemoteReportCreateWorkflowAction(final CreateRemoteReportConfigDialog createRemoteReportConfigDialog, final RemoteReportTableModel remoteReportTableModel, final RemoteReportDialogInputPanel remoteReportDialogInputPanel, boolean isEdit, int changedRow) {
        this.createRemoteReportConfigDialog = createRemoteReportConfigDialog;
        this.remoteReportDialogInputPanel = remoteReportDialogInputPanel;
        this.remoteReportTableModel = remoteReportTableModel;
        this.isEdit = isEdit;
        this.changedRow = changedRow;
    }

    @Override
    public void actionPerformed(final ActionEvent e) {
        RemoteReportConfigDialogData configuration = this.remoteReportDialogInputPanel.getConfiguration();

        RemoteResultCheckerFactory remoteResultCheckerFactory = new RemoteResultCheckerFactory(configuration, remoteReportTableModel, isEdit);

        List<String> allChecks = remoteResultCheckerFactory.performAllChecks();

        if (allChecks.isEmpty()) {
            if (isEdit) {
                remoteReportTableModel.removeConfigurationDataOnRow(changedRow);
            }
            remoteReportTableModel.addRow(configuration);
            createRemoteReportConfigDialog.setVisible(false);
        } else {
            String collect = String.join("\n", allChecks);
            JOptionPane.showMessageDialog(new JFrame(), collect, "Configuration Error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }
}
