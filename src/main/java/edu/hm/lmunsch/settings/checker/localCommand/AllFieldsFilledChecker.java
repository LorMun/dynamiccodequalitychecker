package edu.hm.lmunsch.settings.checker.localCommand;

import edu.hm.lmunsch.settings.configProvider.ParserAndFileLocation;
import edu.hm.lmunsch.settings.entity.CommandConfigDialogData;
import edu.hm.lmunsch.settings.ui.model.CommandConfigTableModel;

public class AllFieldsFilledChecker extends LocalCommandAbstractChecker {

    private final static String RESULTSTRING = "Please make sure all fields are filled";

    @Override
    String getResultString() {
        return RESULTSTRING;
    }

    @Override
    boolean isConfigValid(CommandConfigDialogData commandConfigDialogData, CommandConfigTableModel commandConfigTableModel) {

        if (commandConfigDialogData.getName() == null || commandConfigDialogData.getName().isEmpty() ||
                commandConfigDialogData.getCommand() == null || commandConfigDialogData.getCommand().isEmpty()) {
            return false;
        }

        for (ParserAndFileLocation parserAndFileLocation : commandConfigDialogData.getParserAndFileLocations()) {
            if (parserAndFileLocation.getFilelocation() == null || parserAndFileLocation.getFilelocation().isEmpty() ||
                    parserAndFileLocation.getParserName() == null || parserAndFileLocation.getParserName().isEmpty()) {
                return false;
            }
        }

        return true;
    }
}
