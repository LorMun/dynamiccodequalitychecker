package edu.hm.lmunsch.settings.checker.localCommand;

import edu.hm.lmunsch.settings.entity.CommandConfigDialogData;
import edu.hm.lmunsch.settings.ui.model.CommandConfigTableModel;

public class DuplicateNameChecker extends LocalCommandAbstractChecker {

    private final static String RESULTSTRING = "The name must be unique for all configurations";

    private boolean isEdit;

    public DuplicateNameChecker(boolean isEdit) {
        this.isEdit = isEdit;
    }

    @Override
    String getResultString() {
        return RESULTSTRING;
    }

    @Override
    boolean isConfigValid(CommandConfigDialogData commandConfigDialogData, CommandConfigTableModel commandConfigTableModel) {
        if (isEdit) {
            return true;
        }
        for (CommandConfigDialogData data : commandConfigTableModel.getData()) {
            if (data.getName().equals(commandConfigDialogData.getName())) {
                return false;
            }
        }
        return true;
    }
}
