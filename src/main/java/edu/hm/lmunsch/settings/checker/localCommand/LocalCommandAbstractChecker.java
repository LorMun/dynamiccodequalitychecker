package edu.hm.lmunsch.settings.checker.localCommand;

import edu.hm.lmunsch.settings.entity.CommandConfigDialogData;
import edu.hm.lmunsch.settings.ui.model.CommandConfigTableModel;

public abstract class LocalCommandAbstractChecker {

    private static String RESULTSTRING;

    abstract String getResultString();

    abstract boolean isConfigValid(CommandConfigDialogData commandConfigDialogData,  CommandConfigTableModel commandConfigTableModel);

}
