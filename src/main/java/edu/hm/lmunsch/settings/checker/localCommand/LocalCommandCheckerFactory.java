package edu.hm.lmunsch.settings.checker.localCommand;

import edu.hm.lmunsch.settings.entity.CommandConfigDialogData;
import edu.hm.lmunsch.settings.ui.model.CommandConfigTableModel;

import java.util.ArrayList;
import java.util.List;

public class LocalCommandCheckerFactory {

    private List<LocalCommandAbstractChecker> allCheckers = new ArrayList<>();
    private CommandConfigDialogData commandConfigDialogData;
    private CommandConfigTableModel commandConfigTableModel;
    private boolean isEdit;

    public LocalCommandCheckerFactory(CommandConfigDialogData commandConfigDialogData, CommandConfigTableModel commandConfigTableModel, boolean isEdit) {
        this.commandConfigDialogData = commandConfigDialogData;
        this.commandConfigTableModel = commandConfigTableModel;
        this.isEdit = isEdit;

        allCheckers.add(new AllFieldsFilledChecker());
        allCheckers.add(new DuplicateNameChecker(isEdit));
        allCheckers.add(new NoForbiddenSymbolsChecker());
    }

    public List<String> performAllChecks() {
        List<String> failedCheckMessages = new ArrayList<>();

        for (LocalCommandAbstractChecker check : allCheckers) {
            if (!check.isConfigValid(commandConfigDialogData, commandConfigTableModel)) {
                failedCheckMessages.add(check.getResultString());
            }
        }
        return failedCheckMessages;
    }

}
