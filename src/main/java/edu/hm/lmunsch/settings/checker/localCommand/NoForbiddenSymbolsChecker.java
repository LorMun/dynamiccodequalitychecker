package edu.hm.lmunsch.settings.checker.localCommand;

import edu.hm.lmunsch.settings.configProvider.ParserAndFileLocation;
import edu.hm.lmunsch.settings.entity.CommandConfigDialogData;
import edu.hm.lmunsch.settings.ui.model.CommandConfigTableModel;

public class NoForbiddenSymbolsChecker extends LocalCommandAbstractChecker {

    private final static String RESULTSTRING = "The config can't contain a \"&\" or \";\"-Symbol";

    @Override
    String getResultString() {
        return RESULTSTRING;
    }

    @Override
    boolean isConfigValid(CommandConfigDialogData commandConfigDialogData, CommandConfigTableModel commandConfigTableModel) {
        if (commandConfigDialogData.getName().contains("&")|| commandConfigDialogData.getName().contains(";") ||
                commandConfigDialogData.getCommand().contains("&") || commandConfigDialogData.getCommand().contains(";")) {
            return false;
        }

        for (ParserAndFileLocation parserAndFileLocation : commandConfigDialogData.getParserAndFileLocations()) {
            if (parserAndFileLocation.getFilelocation().contains("&") || parserAndFileLocation.getFilelocation().contains(";") ||
                    parserAndFileLocation.getParserName().contains("&") || parserAndFileLocation.getParserName().contains(";")) {
                return false;
            }
        }
        return true;
    }
}
