package edu.hm.lmunsch.settings.checker.remoteResult;

import edu.hm.lmunsch.settings.entity.RemoteReportConfigDialogData;
import edu.hm.lmunsch.settings.ui.model.RemoteReportTableModel;

public class AllFieldsFilledChecker extends RemoteResultAbstractChecker {

    private final static String RESULTSTRING = "Please make sure all fields are filled";

    @Override
    String getResultString() {
        return RESULTSTRING;
    }

    @Override
    boolean isConfigValid(RemoteReportConfigDialogData commandConfigDialogData, RemoteReportTableModel commandConfigTableModel) {

        if (commandConfigDialogData.getName() == null || commandConfigDialogData.getName().isEmpty() ||
                commandConfigDialogData.getUrl() == null || commandConfigDialogData.getUrl().isEmpty()) {
            return false;
        }

        return true;
    }
}
