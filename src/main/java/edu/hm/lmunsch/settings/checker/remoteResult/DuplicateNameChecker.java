package edu.hm.lmunsch.settings.checker.remoteResult;

import edu.hm.lmunsch.settings.entity.RemoteReportConfigDialogData;
import edu.hm.lmunsch.settings.ui.model.RemoteReportTableModel;

public class DuplicateNameChecker extends RemoteResultAbstractChecker {

    private final static String RESULTSTRING = "The name must be unique for all configurations";

    private boolean isEdit;

    public DuplicateNameChecker(boolean isEdit) {
        this.isEdit = isEdit;
    }

    @Override
    String getResultString() {
        return RESULTSTRING;
    }

    @Override
    boolean isConfigValid(RemoteReportConfigDialogData commandConfigDialogData, RemoteReportTableModel commandConfigTableModel) {
        if (isEdit) {
            return true;
        }
        for (RemoteReportConfigDialogData data : commandConfigTableModel.getData()) {
            if (data.getName().equals(commandConfigDialogData.getName())) {
                return false;
            }
        }
        return true;
    }
}
