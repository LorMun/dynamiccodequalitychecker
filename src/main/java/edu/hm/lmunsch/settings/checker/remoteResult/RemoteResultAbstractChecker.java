package edu.hm.lmunsch.settings.checker.remoteResult;

import edu.hm.lmunsch.settings.entity.RemoteReportConfigDialogData;
import edu.hm.lmunsch.settings.ui.model.RemoteReportTableModel;

public abstract class RemoteResultAbstractChecker {

    private static String RESULTSTRING;

    abstract String getResultString();

    abstract boolean isConfigValid(RemoteReportConfigDialogData commandConfigDialogData, RemoteReportTableModel commandConfigTableModel);

}
