package edu.hm.lmunsch.settings.checker.remoteResult;

import edu.hm.lmunsch.settings.entity.RemoteReportConfigDialogData;
import edu.hm.lmunsch.settings.ui.model.RemoteReportTableModel;

import java.util.ArrayList;
import java.util.List;

public class RemoteResultCheckerFactory {

    private List<RemoteResultAbstractChecker> allCheckers = new ArrayList<>();
    private RemoteReportConfigDialogData remoteReportConfigDialogData;
    private RemoteReportTableModel remoteReportTableModel;
    private boolean isEdit;

    public RemoteResultCheckerFactory(RemoteReportConfigDialogData remoteReportConfigDialogData, RemoteReportTableModel remoteReportTableModel, boolean isEdit) {
        this.remoteReportConfigDialogData = remoteReportConfigDialogData;
        this.remoteReportTableModel = remoteReportTableModel;
        this.isEdit = isEdit;

        allCheckers.add(new AllFieldsFilledChecker());
        allCheckers.add(new DuplicateNameChecker(isEdit));
    }

    public List<String> performAllChecks() {
        List<String> failedCheckMessages = new ArrayList<>();

        for (RemoteResultAbstractChecker check : allCheckers) {
            if (!check.isConfigValid(remoteReportConfigDialogData, remoteReportTableModel)) {
                failedCheckMessages.add(check.getResultString());
            }
        }
        return failedCheckMessages;
    }

}
