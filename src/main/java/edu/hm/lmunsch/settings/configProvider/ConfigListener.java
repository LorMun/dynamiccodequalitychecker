package edu.hm.lmunsch.settings.configProvider;

public interface ConfigListener {

    void configurationChanged();

}
