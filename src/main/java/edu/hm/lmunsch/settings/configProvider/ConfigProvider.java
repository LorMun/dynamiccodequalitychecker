package edu.hm.lmunsch.settings.configProvider;

import com.intellij.openapi.components.PersistentStateComponent;
import com.intellij.openapi.components.State;
import com.intellij.openapi.components.Storage;
import com.intellij.util.xmlb.annotations.MapAnnotation;
import edu.hm.lmunsch.settings.entity.CommandConfigDialogData;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

@State(
        name = "ConfigProviderState",
        storages = {
                @Storage("DynamicCodeQualityChecker-CommandConfig.xml")
        }
)
public class ConfigProvider implements PersistentStateComponent<ConfigProvider.ProjectSettings> {

    private final List<ConfigListener> configListeners = new ArrayList<>();

    private ProjectSettings projectSettings;


    public ConfigProvider() {
        projectSettings = new ProjectSettings();
    }

    @Nullable
    @Override
    public ProjectSettings getState() {
        return projectSettings;
    }

    @Override
    public void loadState(@NotNull ProjectSettings projectSettings) {
        this.projectSettings = projectSettings;
    }

    public void setCurrentConfig(List<CommandConfigDialogData> currentConfig) {
        this.projectSettings = ProjectSettings.create(currentConfig);
        publishConfigChanged();
    }

    public void addConfigurationListener(ConfigListener configListener) {
        configListeners.add(configListener);
    }

    private void publishConfigChanged() {
        for (ConfigListener configListener : configListeners) {
            configListener.configurationChanged();
        }
    }

    public static class ProjectSettings {

        @MapAnnotation
        private Map<String, CommandConfigDialogData> configuration;

        public ProjectSettings() {
        }

        static ProjectSettings create(@NotNull final List<CommandConfigDialogData> dialogDataList) {
            final Map<String, CommandConfigDialogData> mapForSerialization = new TreeMap<>();
            for (CommandConfigDialogData dialogData: dialogDataList) {
                mapForSerialization.put(dialogData.getName(), dialogData);
            }

            final ProjectSettings projectSettings = new ProjectSettings();
            projectSettings.configuration = mapForSerialization;
            return projectSettings;
        }

        @NotNull
        public Map<String, CommandConfigDialogData> configuration() {
            if (configuration == null) {
                return new TreeMap<>();
            }
            return configuration;
        }

        public CommandConfigDialogData getConfigurationForName(String name) {
            if (configuration.containsKey(name)) {
                return configuration.get(name);
            } else {
                return null;
            }
        }

        public Map<String, CommandConfigDialogData> getConfiguration() {
            return configuration;
        }
    }

}
