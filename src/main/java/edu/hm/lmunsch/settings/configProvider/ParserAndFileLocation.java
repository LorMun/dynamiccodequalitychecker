package edu.hm.lmunsch.settings.configProvider;

public class ParserAndFileLocation {
    private final String filelocation;
    private final String parserName;

    public ParserAndFileLocation(String parserName, String filelocation) {
        this.filelocation = filelocation;
        this.parserName = parserName;
    }

    public String getFilelocation() {
        return filelocation;
    }

    public String getParserName() {
        return parserName;
    }

}
