package edu.hm.lmunsch.settings.configProvider;

import com.intellij.util.xmlb.Converter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ParserAndFileLocationConverter extends Converter<List<ParserAndFileLocation>> {

    @Nullable
    @Override
    public List<ParserAndFileLocation> fromString(@NotNull String s) {
        List<ParserAndFileLocation> result = new ArrayList<>();
        String[] split = s.split(";");
        for (String combi : split) {
            String[] split1 = combi.split("&");
            if (split1.length == 2) {
                result.add(new ParserAndFileLocation(split1[0], split1[1]));
            }
        }
        return result;
    }

    @Nullable
    @Override
    public String toString(@NotNull List<ParserAndFileLocation> parserAndFileLocations) {
        return parserAndFileLocations.stream()
                .map(parserAndFileLocation -> parserAndFileLocation.getParserName().concat("&").concat(parserAndFileLocation.getFilelocation()))
                .collect(Collectors.joining(";"));
    }
}

