package edu.hm.lmunsch.settings.configProvider;

import com.intellij.openapi.components.PersistentStateComponent;
import com.intellij.openapi.components.State;
import com.intellij.openapi.components.Storage;
import com.intellij.util.xmlb.annotations.MapAnnotation;
import edu.hm.lmunsch.settings.entity.RemoteReportConfigDialogData;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

@State(
        name = "RemoteReportConfigProviderState",
        storages = {
                @Storage("DynamicCodeQualityChecker-RemoteConfig.xml")
        }
)
public class RemoteReportConfigProvider implements PersistentStateComponent<RemoteReportConfigProvider.RemoteReportProjectSettings> {

    private final List<ConfigListener> configListeners = new ArrayList<>();

    private RemoteReportProjectSettings remoteReportProjectSettings;


    public RemoteReportConfigProvider() {
        remoteReportProjectSettings = new RemoteReportProjectSettings();
    }

    @Nullable
    @Override
    public RemoteReportProjectSettings getState() {
        return remoteReportProjectSettings;
    }

    @Override
    public void loadState(@NotNull RemoteReportProjectSettings remoteReportProjectSettings) {
        this.remoteReportProjectSettings = remoteReportProjectSettings;
    }

    public void setCurrentConfig(List<RemoteReportConfigDialogData> currentConfig) {
        this.remoteReportProjectSettings = RemoteReportProjectSettings.create(currentConfig);
        publishConfigChanged();

    }

    public void addConfigurationListener(ConfigListener configListener) {
        configListeners.add(configListener);
    }

    private void publishConfigChanged() {
        for (ConfigListener configListener : configListeners) {
            configListener.configurationChanged();
        }
    }

    public static class RemoteReportProjectSettings {

        @MapAnnotation
        private Map<String, RemoteReportConfigDialogData> configuration;

        public RemoteReportProjectSettings() {
        }

        static RemoteReportProjectSettings create(@NotNull final List<RemoteReportConfigDialogData> dialogDataList) {
            final Map<String, RemoteReportConfigDialogData> mapForSerialization = new TreeMap<>();
            for (RemoteReportConfigDialogData dialogData: dialogDataList) {
                mapForSerialization.put(dialogData.getName(), dialogData);
            }

            final RemoteReportProjectSettings remoteReportProjectSettings = new RemoteReportProjectSettings();
            remoteReportProjectSettings.configuration = mapForSerialization;
            return remoteReportProjectSettings;
        }

        @NotNull
        public Map<String, RemoteReportConfigDialogData> configuration() {
            if (configuration == null) {
                return new TreeMap<>();
            }
            return configuration;
        }

        public RemoteReportConfigDialogData getConfigurationForName(String name) {
            if (configuration.containsKey(name)) {
                return configuration.get(name);
            } else {
                return null;
            }
        }

        public Map<String, RemoteReportConfigDialogData> getConfiguration() {
            return configuration;
        }
    }

}
