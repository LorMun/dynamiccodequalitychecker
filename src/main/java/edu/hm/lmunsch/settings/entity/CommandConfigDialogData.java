package edu.hm.lmunsch.settings.entity;


import com.intellij.util.xmlb.annotations.OptionTag;
import edu.hm.hafner.analysis.registry.ParserDescriptor;
import edu.hm.lmunsch.settings.configProvider.ParserAndFileLocation;
import edu.hm.lmunsch.settings.configProvider.ParserAndFileLocationConverter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CommandConfigDialogData {

    @OptionTag
    private String name;

    @OptionTag
    private String command;

    @OptionTag(converter = ParserAndFileLocationConverter.class)
    private List<ParserAndFileLocation> parserAndFileLocations;

    public CommandConfigDialogData() {
    }

    public CommandConfigDialogData(final String name, final String command, final String filelocation, final String parserName) {
        this.name = name;
        this.command = command;
        this.parserAndFileLocations = new ArrayList<>();
        this.parserAndFileLocations.add(new ParserAndFileLocation(parserName, filelocation));
    }

    public CommandConfigDialogData(final String name, final String command, List<ParserAndFileLocation> parserAndFileLocationList) {
        this.name = name;
        this.command = command;
        this.parserAndFileLocations = parserAndFileLocationList;
    }

    public String getName() {
        return name;
    }

    public String getCommand() {
        return command;
    }

    public List<ParserAndFileLocation> getParserAndFileLocations() {
        return parserAndFileLocations;
    }

    public Optional<ParserAndFileLocation> getParserAndFileLocationByDescriptor(ParserDescriptor descriptor) {
        for (ParserAndFileLocation parserAndFileLocation : this.parserAndFileLocations) {
            if (parserAndFileLocation.getParserName().equals(descriptor.getName())) {
                return Optional.of(parserAndFileLocation);
            }
        }
        return Optional.empty();
    }
}
