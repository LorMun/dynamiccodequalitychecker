package edu.hm.lmunsch.settings.entity;


import com.intellij.util.xmlb.annotations.OptionTag;

public class RemoteReportConfigDialogData {

    @OptionTag
    private String name;

    @OptionTag
    private String url;

    public RemoteReportConfigDialogData() {
    }

    public RemoteReportConfigDialogData(final String name, final String url) {
        this.name = name;
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }

}
