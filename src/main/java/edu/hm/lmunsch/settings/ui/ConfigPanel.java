package edu.hm.lmunsch.settings.ui;

import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.options.Configurable;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.NlsContexts;
import edu.hm.lmunsch.settings.configProvider.ConfigProvider;
import edu.hm.lmunsch.settings.configProvider.RemoteReportConfigProvider;
import edu.hm.lmunsch.settings.entity.CommandConfigDialogData;
import edu.hm.lmunsch.settings.entity.RemoteReportConfigDialogData;
import edu.hm.lmunsch.settings.ui.model.CommandConfigTableModel;
import edu.hm.lmunsch.settings.ui.model.RemoteReportTableModel;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class ConfigPanel implements Configurable {

    private final ConfigPanelContent configPanelContent;

    public ConfigPanel(@NotNull final Project project) {
        this.configPanelContent = new ConfigPanelContent(project);
        fillCommandTable();
        fillRemoteReportTable();
    }

    private void fillCommandTable() {
        ConfigProvider configProvider = ServiceManager.getService(ConfigProvider.class);

        ConfigProvider.ProjectSettings projectSettings = configProvider.getState();
        if (projectSettings.getConfiguration() != null) {
            Map<String, CommandConfigDialogData> existingConfig = projectSettings.getConfiguration();
            List<CommandConfigDialogData> result = new ArrayList<>();
            for (String key : existingConfig.keySet()) {
                result.add(existingConfig.get(key));
            }

            CommandConfigTableModel commandConfigTableModel = configPanelContent.getCommandConfigTableModel();
            commandConfigTableModel.setData(result);
        }
    }

    private void fillRemoteReportTable() {
        RemoteReportConfigProvider configProvider = ServiceManager.getService(RemoteReportConfigProvider.class);

        RemoteReportConfigProvider.RemoteReportProjectSettings state = configProvider.getState();
        if (state.getConfiguration() != null) {
            Map<String, RemoteReportConfigDialogData> existingConfig = state.getConfiguration();
            List<RemoteReportConfigDialogData> result = new ArrayList<>();
            for (String key : existingConfig.keySet()) {
                result.add(existingConfig.get(key));
            }

            RemoteReportTableModel remoteReportTableModel = configPanelContent.getRemoteReportTableModel();
            remoteReportTableModel.setData(result);
        }
    }

    @Override
    public @NlsContexts.ConfigurableName String getDisplayName() {
        return "Dynamic Code Quality Checker";
    }

    @Override
    public JComponent createComponent() {
        return configPanelContent;
    }

    @Override
    public boolean isModified() {
        return true;
    }

    @Override
    public void apply() {
        ConfigProvider configProvider = ServiceManager.getService(ConfigProvider.class);
        List<CommandConfigDialogData> rows = configPanelContent.getCommandConfigTableModel().getData();
        configProvider.setCurrentConfig(rows);

        RemoteReportConfigProvider remoteReportConfigProvider = ServiceManager.getService(RemoteReportConfigProvider.class);
        List<RemoteReportConfigDialogData> remoteRows = configPanelContent.getRemoteReportTableModel().getData();
        remoteReportConfigProvider.setCurrentConfig(remoteRows);

    }
}
