package edu.hm.lmunsch.settings.ui;

import com.intellij.openapi.project.Project;
import com.intellij.ui.TitledSeparator;
import com.intellij.ui.ToolbarDecorator;
import com.intellij.ui.table.JBTable;
import com.intellij.util.ui.JBUI;
import edu.hm.lmunsch.settings.ui.model.CommandConfigTableModel;
import edu.hm.lmunsch.settings.ui.model.RemoteReportTableModel;
import edu.hm.lmunsch.settings.action.localCommandActions.CreateNewConfigEntryAction;
import edu.hm.lmunsch.settings.action.localCommandActions.EditExistingConfigEntryAction;
import edu.hm.lmunsch.settings.action.localCommandActions.RemoveCommandAction;
import edu.hm.lmunsch.settings.action.remoteReportActions.CreateNewRemoteReportConfigEntryAction;
import edu.hm.lmunsch.settings.action.remoteReportActions.EditExistingRemoteReportConfigEntryAction;
import edu.hm.lmunsch.settings.action.remoteReportActions.RemoveRemoteReportAction;

import javax.swing.*;
import javax.swing.table.TableColumn;
import java.awt.*;

public class ConfigPanelContent extends JPanel {

    private static final int NAME_COL_MIN_WIDTH = 80;
    private static final int NAME_COL_MAX_WIDTH = 250;
    private static final int COMMAND_COL_MIN_WIDTH = 100;
    private static final int COMMAND_COL_MAX_WIDTH = 400;

    private static final Dimension DECORATOR_DIMENSIONS = new Dimension(300, 50);

    Project project;
    private static final Insets COMPONENT_INSETS = JBUI.insets(4);

    private final CommandConfigTableModel commandConfigTableModel = new CommandConfigTableModel(project);
    private final JBTable commandTable = new JBTable(commandConfigTableModel);

    private final RemoteReportTableModel remoteReportTableModel = new RemoteReportTableModel(project);
    private final JBTable remoteReportTable = new JBTable(remoteReportTableModel);


    public ConfigPanelContent(final Project project) {
        super(new BorderLayout());

        this.project = project;
        initialise();
    }

    private void initialise() {
        super.invalidate();
        super.repaint();
        add(buildConfigPanel(), BorderLayout.CENTER);
    }

    private JPanel buildConfigPanel() {
        final JPanel configFilePanel = new JPanel(new GridBagLayout());
        configFilePanel.setOpaque(false);

        configFilePanel.add(buildCommandConfigTable(), new GridBagConstraints(
                0, 2, 4, 1, 1.0, 1.0, GridBagConstraints.WEST,
                GridBagConstraints.BOTH, COMPONENT_INSETS, 0, 0));

        configFilePanel.add(buildRemoteReportTable(), new GridBagConstraints(
                0, 3, 4, 1, 1.0, 1.0, GridBagConstraints.WEST,
                GridBagConstraints.BOTH, COMPONENT_INSETS, 0, 0));

        return configFilePanel;
    }

    private JPanel buildCommandConfigTable() {
        setColumnWith(commandTable, 0, NAME_COL_MIN_WIDTH, NAME_COL_MAX_WIDTH, NAME_COL_MAX_WIDTH);
        setColumnWith(commandTable, 1, COMMAND_COL_MIN_WIDTH, COMMAND_COL_MAX_WIDTH, COMMAND_COL_MAX_WIDTH);
        commandTable.setAutoResizeMode(JTable.AUTO_RESIZE_NEXT_COLUMN);
        commandTable.setStriped(true);
        commandTable.getTableHeader().setReorderingAllowed(false);

        final ToolbarDecorator tableDecorator = ToolbarDecorator.createDecorator(commandTable);
        tableDecorator.setAddAction(new CreateNewConfigEntryAction(project, commandConfigTableModel));
        tableDecorator.setRemoveAction(new RemoveCommandAction(project, commandConfigTableModel, commandTable));
        tableDecorator.setEditAction(new EditExistingConfigEntryAction(project, commandConfigTableModel, commandTable));
        tableDecorator.setPreferredSize(DECORATOR_DIMENSIONS);

        final JPanel container = new JPanel(new BorderLayout());
        container.add(new TitledSeparator("Configure Commands"), BorderLayout.NORTH);
        container.add(tableDecorator.createPanel(), BorderLayout.CENTER);

        return container;
    }

    private JPanel buildRemoteReportTable() {
        setColumnWith(remoteReportTable, 0, NAME_COL_MIN_WIDTH, NAME_COL_MAX_WIDTH, NAME_COL_MAX_WIDTH);
        remoteReportTable.setAutoResizeMode(JTable.AUTO_RESIZE_NEXT_COLUMN);
        remoteReportTable.setStriped(true);
        remoteReportTable.getTableHeader().setReorderingAllowed(false);

        final ToolbarDecorator tableDecorator = ToolbarDecorator.createDecorator(remoteReportTable);
        tableDecorator.setAddAction(new CreateNewRemoteReportConfigEntryAction(project, remoteReportTableModel));
        tableDecorator.setRemoveAction(new RemoveRemoteReportAction(project, remoteReportTableModel, remoteReportTable));
        tableDecorator.setEditAction(new EditExistingRemoteReportConfigEntryAction(project, remoteReportTableModel, remoteReportTable));
        tableDecorator.setPreferredSize(DECORATOR_DIMENSIONS);

        final JPanel container = new JPanel(new BorderLayout());
        container.add(new TitledSeparator("Load Remote Report"), BorderLayout.NORTH);
        container.add(tableDecorator.createPanel(), BorderLayout.CENTER);

        return container;
    }

    private void setColumnWith(final JTable table,
                               final int columnIndex,
                               final int minSize,
                               final int preferredSize,
                               final Integer maxSize) {
        final TableColumn column = table.getColumnModel().getColumn(columnIndex);
        column.setMinWidth(minSize);
        column.setWidth(preferredSize);
        column.setPreferredWidth(preferredSize);
        if (maxSize != null) {
            column.setMaxWidth(maxSize);
        }
    }

    public CommandConfigTableModel getCommandConfigTableModel() {
        return commandConfigTableModel;
    }

    public RemoteReportTableModel getRemoteReportTableModel() {
        return remoteReportTableModel;
    }
}
