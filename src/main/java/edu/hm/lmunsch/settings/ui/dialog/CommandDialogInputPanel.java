package edu.hm.lmunsch.settings.ui.dialog;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.ComboBox;
import com.intellij.util.ui.JBUI;
import edu.hm.hafner.analysis.registry.ParserRegistry;
import edu.hm.lmunsch.settings.configProvider.ParserAndFileLocation;
import edu.hm.lmunsch.settings.entity.CommandConfigDialogData;
import edu.hm.lmunsch.settings.action.localCommandActions.AddDynmicDialogPartAction;
import edu.hm.lmunsch.settings.action.localCommandActions.BrowseAction;
import edu.hm.lmunsch.settings.action.localCommandActions.RemoveDynamicDialogPartAction;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CommandDialogInputPanel extends JPanel {

    private final Project project;
    private final CreateCommandDialog createCommandDialog;

    private static final Insets COMPONENT_INSETS = JBUI.insets(4);
    private final JTextField nameField = new JTextField();
    private final JTextField commandField = new JTextField();

    private final JTextField fileLocationField = new JTextField(20);
    private final JButton browseButton;
    private final JButton addDynamicPartButton;

    private final ComboBox parserTypeCombo = new ComboBox();
    private final DefaultComboBoxModel parserTypeModel;

    final JLabel fileLocationLabel = new JLabel("Result File");
    final JLabel parserTypeLabel = new JLabel("Parser");
    ParserRegistry parserRegistry;

    private final Map<String, ParserAndResultfileFieldHelper> dynamicGuiParts = new HashMap<>();
    int dynamicPartRowCounter = 0;
    int dynamicPartCounter = 0;

    public CommandDialogInputPanel(Project project, CreateCommandDialog createCommandDialog) {
        super(new GridBagLayout());

        this.createCommandDialog = createCommandDialog;

        parserRegistry = new ParserRegistry();

        parserTypeModel = new DefaultComboBoxModel(parserRegistry.getNames().toArray());

        this.project = project;
        this.browseButton = new JButton(new BrowseAction(project, this, fileLocationField));
        this.addDynamicPartButton = new JButton(new AddDynmicDialogPartAction(this, createCommandDialog));

        initialize();
    }

    private void initialize() {
        parserTypeCombo.setModel(parserTypeModel);

        final JLabel nameLabel = new JLabel("Name");
        nameField.setToolTipText("Enter an identifier");

        final JLabel commandLabel = new JLabel("Command");
        nameField.setToolTipText("Enter the Command to execute");

        fileLocationField.setToolTipText("Location for the Result file");
        fileLocationField.setEnabled(true);

        parserTypeLabel.setToolTipText("Pick your parser");

        browseButton.setText("Browse");
        addDynamicPartButton.setText("Add");
        add(nameLabel, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.EAST, GridBagConstraints.NONE, COMPONENT_INSETS, 0, 0));
        add(nameField, new GridBagConstraints(1, 0, 2, 1, 1.0, 0.0,
                GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, COMPONENT_INSETS, 0, 0));

        add(commandLabel, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
                GridBagConstraints.EAST, GridBagConstraints.NONE, COMPONENT_INSETS, 0, 0));
        add(commandField, new GridBagConstraints(1, 1, 2, 1, 1.0, 0.0,
                GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, COMPONENT_INSETS, 0, 0));

        add(Box.createRigidArea(new Dimension(15, 0)), new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
                GridBagConstraints.EAST, GridBagConstraints.NONE, COMPONENT_INSETS, 0, 0));

        add(fileLocationLabel, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
                GridBagConstraints.EAST, GridBagConstraints.NONE, COMPONENT_INSETS, 0, 0));
        add(fileLocationField, new GridBagConstraints(1, 3, 1, 1, 1.0, 0.0,
                GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, COMPONENT_INSETS, 0, 0));
        add(browseButton, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0,
                GridBagConstraints.WEST, GridBagConstraints.NONE, COMPONENT_INSETS, 0, 0));
        add(addDynamicPartButton, new GridBagConstraints(3, 3, 1, 1, 0.0, 0.0,
                GridBagConstraints.WEST, GridBagConstraints.NONE, COMPONENT_INSETS, 0, 0));

        add(parserTypeLabel, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
                GridBagConstraints.EAST, GridBagConstraints.NONE, COMPONENT_INSETS, 0, 0));
        add(parserTypeCombo, new GridBagConstraints(1, 4, 2, 1, 1.0, 0.0,
                GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, COMPONENT_INSETS, 0, 0));
    }

    public CommandConfigDialogData getConfiguration() {
        List<ParserAndFileLocation> parserAndFileLocations = new ArrayList<>();
        parserAndFileLocations.add(new ParserAndFileLocation((String) parserTypeCombo.getSelectedItem(), fileLocationField.getText()));

        for (String key : dynamicGuiParts.keySet()) {
            ParserAndResultfileFieldHelper config = dynamicGuiParts.get(key);
            parserAndFileLocations.add(new ParserAndFileLocation((String) config.getParserType().getSelectedItem(), config.resultfileName.getText()));
        }

        return new CommandConfigDialogData(nameField.getText(), commandField.getText(), parserAndFileLocations);
    }

    public ParserAndResultfileFieldHelper addDynamicDialogPart() {
        final JLabel tempFileLocationLabel = new JLabel("Result File");
        final JTextField tempFileLocationField = new JTextField(20);
        tempFileLocationField.setEnabled(true);

        final JLabel tempParserTypeLabel = new JLabel("Parser");
        DefaultComboBoxModel parserTypeSelector = new DefaultComboBoxModel(parserRegistry.getNames().toArray());
        final ComboBox parserTypeCombo = new ComboBox();
        parserTypeCombo.setModel(parserTypeSelector);

        final JButton newBrowseButton = new JButton(new BrowseAction(project, this, tempFileLocationField));
        newBrowseButton.setText("Browse");

        String removeButtonName = "removeButton" + dynamicPartCounter;
        final JButton newRemoveButton = new JButton(new RemoveDynamicDialogPartAction(this, createCommandDialog, removeButtonName));
        newRemoveButton.setText("Remove");

        Component seperatorSpace = Box.createRigidArea(new Dimension(15, 0));

        ParserAndResultfileFieldHelper parserAndResultfileFieldHelper = new ParserAndResultfileFieldHelper(tempFileLocationLabel, tempFileLocationField, tempParserTypeLabel, parserTypeSelector, parserTypeCombo, newBrowseButton, newRemoveButton, seperatorSpace);

        dynamicGuiParts.put(removeButtonName, parserAndResultfileFieldHelper);


        add(seperatorSpace, new GridBagConstraints(1, 5 + dynamicPartRowCounter, 1, 1, 0.0, 0.0,
                GridBagConstraints.EAST, GridBagConstraints.NONE, COMPONENT_INSETS, 0, 0));

        add(tempFileLocationLabel, new GridBagConstraints(0, 6 + dynamicPartRowCounter, 1, 1, 0.0, 0.0,
                GridBagConstraints.EAST, GridBagConstraints.NONE, COMPONENT_INSETS, 0, 0));
        add(tempFileLocationField, new GridBagConstraints(1, 6 + dynamicPartRowCounter, 1, 1, 1.0, 0.0,
                GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, COMPONENT_INSETS, 0, 0));
        add(newBrowseButton, new GridBagConstraints(2, 6 + dynamicPartRowCounter, 1, 1, 0.0, 0.0,
                GridBagConstraints.WEST, GridBagConstraints.NONE, COMPONENT_INSETS, 0, 0));
        add(newRemoveButton, new GridBagConstraints(3, 6 + dynamicPartRowCounter, 1, 1, 0.0, 0.0,
                GridBagConstraints.WEST, GridBagConstraints.NONE, COMPONENT_INSETS, 0, 0));

        add(tempParserTypeLabel, new GridBagConstraints(0, 7 + dynamicPartRowCounter, 1, 1, 0.0, 0.0,
                GridBagConstraints.EAST, GridBagConstraints.NONE, COMPONENT_INSETS, 0, 0));
        add(parserTypeCombo, new GridBagConstraints(1, 7 + dynamicPartRowCounter, 2, 1, 1.0, 0.0,
                GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, COMPONENT_INSETS, 0, 0));
        dynamicPartRowCounter = dynamicPartRowCounter + 3;
        dynamicPartCounter++;

        return parserAndResultfileFieldHelper;
    }

    public JTextField getFileLocationField() {
        return fileLocationField;
    }

    public void reloadDialog() {
        super.revalidate();
    }

    public void removeDynamicDialogPart(String deleteButtonName) {
        for (String key : dynamicGuiParts.keySet()) {
            if (key.equals(deleteButtonName)) {
                ParserAndResultfileFieldHelper parserAndResultfileFieldHelper = dynamicGuiParts.get(key);
                remove(parserAndResultfileFieldHelper.tempFileLocationLabel);
                remove(parserAndResultfileFieldHelper.resultfileName);
                remove(parserAndResultfileFieldHelper.newBrowseButton);
                remove(parserAndResultfileFieldHelper.tempParserTypeLabel);
                remove(parserAndResultfileFieldHelper.parserTypeCombo);
                remove(parserAndResultfileFieldHelper.removeButton);
                remove(parserAndResultfileFieldHelper.seperatorSpace);
                reloadDialog();
            }
        }
        dynamicGuiParts.remove(deleteButtonName);
    }

    public void prefilValue(CommandConfigDialogData commandConfigDialogData) {
        nameField.setText(commandConfigDialogData.getName());
        commandField.setText(commandConfigDialogData.getCommand());
        List<ParserAndFileLocation> parserAndFileLocations = commandConfigDialogData.getParserAndFileLocations();
        ParserAndFileLocation parserAndFileLocation = parserAndFileLocations.get(0);
        parserTypeModel.setSelectedItem(parserAndFileLocation.getParserName());
        fileLocationField.setText(parserAndFileLocation.getFilelocation());

        if(parserAndFileLocations.size() > 1) {
            for (int i = 1; i < parserAndFileLocations.size(); i++) {
                ParserAndResultfileFieldHelper parserAndResultfileFieldHelper = addDynamicDialogPart();
                parserAndResultfileFieldHelper.getResultfileName().setText(parserAndFileLocations.get(i).getFilelocation());
                parserAndResultfileFieldHelper.getParserTypeCombo().setSelectedItem(parserAndFileLocations.get(i).getParserName());
            }
        }
    }

    public static class ParserAndResultfileFieldHelper {
        private final JLabel tempFileLocationLabel;
        private final JTextField resultfileName;

        private final JLabel tempParserTypeLabel;
        private final DefaultComboBoxModel parserType;
        private final ComboBox parserTypeCombo;

        private final JButton newBrowseButton;
        private final JButton removeButton;

        private final Component seperatorSpace;


        public ParserAndResultfileFieldHelper(JLabel tempFileLocationLabel, JTextField resultfileName, JLabel tempParserTypeLabel, DefaultComboBoxModel parserType, ComboBox parserTypeCombo, JButton newBrowseButton, JButton removeButton, Component seperatorSpace) {
            this.tempFileLocationLabel = tempFileLocationLabel;
            this.resultfileName = resultfileName;
            this.tempParserTypeLabel = tempParserTypeLabel;
            this.parserType = parserType;
            this.parserTypeCombo = parserTypeCombo;
            this.newBrowseButton = newBrowseButton;
            this.removeButton = removeButton;
            this.seperatorSpace = seperatorSpace;
        }

        public JLabel getTempFileLocationLabel() {
            return tempFileLocationLabel;
        }

        public JTextField getResultfileName() {
            return resultfileName;
        }

        public JLabel getTempParserTypeLabel() {
            return tempParserTypeLabel;
        }

        public DefaultComboBoxModel getParserType() {
            return parserType;
        }

        public JButton getNewBrowseButton() {
            return newBrowseButton;
        }

        public ComboBox getParserTypeCombo() {
            return parserTypeCombo;
        }
    }

}
