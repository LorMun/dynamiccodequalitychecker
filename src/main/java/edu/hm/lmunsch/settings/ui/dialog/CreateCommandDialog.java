package edu.hm.lmunsch.settings.ui.dialog;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.SystemInfo;
import com.intellij.util.ui.JBUI;
import edu.hm.lmunsch.settings.action.localCommandActions.FinishLocalCommandCreateWorkflowAction;
import edu.hm.lmunsch.settings.entity.CommandConfigDialogData;
import edu.hm.lmunsch.settings.ui.model.CommandConfigTableModel;

import javax.swing.*;
import java.awt.*;

public class CreateCommandDialog extends JDialog {

    private static final Insets COMPONENT_INSETS = JBUI.insets(4);

    private final Project project;

    private final CommandDialogInputPanel commandDialogInputPanel;

    private JButton commitButton;

    private final CommandConfigTableModel commandConfigTableModel;

    private final boolean isEdit;

    public CreateCommandDialog(final Project project, CommandConfigTableModel commandConfigTableModel, boolean isEdit, int selectedRow) {
        this.project = project;
        this.commandConfigTableModel = commandConfigTableModel;
        this.commandDialogInputPanel = new CommandDialogInputPanel(project, this);
        this.isEdit = isEdit;
        initialize(selectedRow);

    }

    private void initialize(int selectedRow) {
        setLayout(new BorderLayout());
        setLocationRelativeTo(null);
        setMinimumSize(new Dimension(500, 400));
        setPreferredSize(new Dimension(900, 450));
        setModal(true);
        pack();

        commitButton = new JButton(new FinishLocalCommandCreateWorkflowAction(this, commandConfigTableModel, commandDialogInputPanel, isEdit, selectedRow));

        final JPanel bottomPanel = new JPanel(new GridBagLayout());
        bottomPanel.setBorder(JBUI.Borders.empty(4, 8, 8, 8));

        if (SystemInfo.isMac) {
            bottomPanel.add(Box.createHorizontalGlue(), new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0,
                    GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, COMPONENT_INSETS, 0, 0));
        } else {
            bottomPanel.add(Box.createHorizontalGlue(), new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0,
                    GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, COMPONENT_INSETS, 0, 0));
        }

        bottomPanel.add(commitButton, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.EAST, GridBagConstraints.NONE, COMPONENT_INSETS, 0, 0));

        add(bottomPanel, BorderLayout.SOUTH);

        bottomPanel.add(commitButton, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.EAST, GridBagConstraints.NONE, COMPONENT_INSETS, 0, 0));

        final Toolkit toolkit = Toolkit.getDefaultToolkit();
        setLocation((toolkit.getScreenSize().width - getSize().width) / 2,
                (toolkit.getScreenSize().height - getSize().height) / 2);

        add(commandDialogInputPanel, BorderLayout.CENTER);
        commitButton.setText("Commit");
    }

    public void increaseHeigth() {
        increaseHeigth(1);
    }

    public void increaseHeigth(int n) {
        Dimension preferredSize = getPreferredSize();
        int height = preferredSize.height;
        int width = preferredSize.width;
        setPreferredSize(new Dimension(width, height + 80 * n));
        pack();
    }

    public void decreaseHeigth() {
        Dimension preferredSize = getPreferredSize();
        int height = preferredSize.height;
        int width = preferredSize.width;
        setPreferredSize(new Dimension(width, height - 80));
        pack();
    }

    public void prefilValues(CommandConfigDialogData commandConfigDialogData) {
        increaseHeigth(commandConfigDialogData.getParserAndFileLocations().size());

        commandDialogInputPanel.prefilValue(commandConfigDialogData);
    }

}
