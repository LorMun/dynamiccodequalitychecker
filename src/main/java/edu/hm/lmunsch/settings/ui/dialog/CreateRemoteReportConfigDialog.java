package edu.hm.lmunsch.settings.ui.dialog;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.SystemInfo;
import com.intellij.util.ui.JBUI;
import edu.hm.lmunsch.settings.action.remoteReportActions.FinishRemoteReportCreateWorkflowAction;
import edu.hm.lmunsch.settings.entity.RemoteReportConfigDialogData;
import edu.hm.lmunsch.settings.ui.model.RemoteReportTableModel;

import javax.swing.*;
import java.awt.*;

public class CreateRemoteReportConfigDialog extends JDialog {

    private static final Insets COMPONENT_INSETS = JBUI.insets(4);

    private final Project project;

    private final RemoteReportDialogInputPanel remoteReportDialogInputPanel;

    private JButton commitButton;

    private final RemoteReportTableModel remoteReportTableModel;

    private final boolean isEdit;

    public CreateRemoteReportConfigDialog(final Project project, RemoteReportTableModel remoteReportTableModel, boolean isEdit, int selectedRow) {
        this.project = project;
        this.remoteReportTableModel = remoteReportTableModel;
        this.remoteReportDialogInputPanel = new RemoteReportDialogInputPanel(project, this);
        this.isEdit = isEdit;
        initialize(selectedRow);
    }

    private void initialize(int selectedRow) {
        setLayout(new BorderLayout());
        setLocationRelativeTo(null);
        setMinimumSize(new Dimension(500, 400));
        setPreferredSize(new Dimension(900, 450));
        setModal(true);
        pack();

        commitButton = new JButton(new FinishRemoteReportCreateWorkflowAction(this, remoteReportTableModel, remoteReportDialogInputPanel, isEdit, selectedRow));

        final JPanel bottomPanel = new JPanel(new GridBagLayout());
        bottomPanel.setBorder(JBUI.Borders.empty(4, 8, 8, 8));

        if (SystemInfo.isMac) {
            bottomPanel.add(Box.createHorizontalGlue(), new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0,
                    GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, COMPONENT_INSETS, 0, 0));
        } else {
            bottomPanel.add(Box.createHorizontalGlue(), new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0,
                    GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, COMPONENT_INSETS, 0, 0));
        }

        bottomPanel.add(commitButton, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.EAST, GridBagConstraints.NONE, COMPONENT_INSETS, 0, 0));
        add(bottomPanel, BorderLayout.SOUTH);

        bottomPanel.add(commitButton, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.EAST, GridBagConstraints.NONE, COMPONENT_INSETS, 0, 0));

        final Toolkit toolkit = Toolkit.getDefaultToolkit();
        setLocation((toolkit.getScreenSize().width - getSize().width) / 2,
                (toolkit.getScreenSize().height - getSize().height) / 2);

        add(remoteReportDialogInputPanel, BorderLayout.CENTER);
        commitButton.setText("Commit");
    }

    public void prefilValues(RemoteReportConfigDialogData remoteReportConfigDialogData) {
        remoteReportDialogInputPanel.prefilValue(remoteReportConfigDialogData);
    }

}
