package edu.hm.lmunsch.settings.ui.dialog;

import com.intellij.openapi.project.Project;
import com.intellij.util.ui.JBUI;
import edu.hm.lmunsch.settings.entity.RemoteReportConfigDialogData;

import javax.swing.*;
import java.awt.*;

public class RemoteReportDialogInputPanel extends JPanel {

    private final Project project;
    private final CreateRemoteReportConfigDialog createCommandDialog;

    private static final Insets COMPONENT_INSETS = JBUI.insets(2);
    private final JTextField nameField = new JTextField();
    private final JTextField urlField = new JTextField();


    public RemoteReportDialogInputPanel(Project project, CreateRemoteReportConfigDialog createCommandDialog) {
        super(new GridBagLayout());

        this.createCommandDialog = createCommandDialog;
        this.project = project;

        initialize();
    }

    private void initialize() {

        final JLabel nameLabel = new JLabel("Name");
        nameField.setToolTipText("Enter an identifier");

        final JLabel urlLabel = new JLabel("URL");
        nameField.setToolTipText("Enter the URL to load the Report");

        final JLabel urlInfoLabel = new JLabel("The URL must be in format: http(s)://<domain>/job/<project-name>/");

        add(nameLabel, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.EAST, GridBagConstraints.NONE, COMPONENT_INSETS, 0, 0));
        add(nameField, new GridBagConstraints(1, 0, 2, 1, 1.0, 0.0,
                GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, COMPONENT_INSETS, 0, 0));

        add(urlLabel, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
                GridBagConstraints.EAST, GridBagConstraints.NONE, COMPONENT_INSETS, 0, 0));
        add(urlField, new GridBagConstraints(1, 1, 2, 1, 1.0, 0.0,
                GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, COMPONENT_INSETS, 0, 0));
        add(urlInfoLabel, new GridBagConstraints(1, 2, 2, 1, 1.0, 0.0,
                GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, COMPONENT_INSETS, 0, 0));


    }

    public RemoteReportConfigDialogData getConfiguration() {
        return new RemoteReportConfigDialogData(nameField.getText(), urlField.getText());
    }

    public void prefilValue(RemoteReportConfigDialogData remoteReportConfigDialogData) {
        nameField.setText(remoteReportConfigDialogData.getName());
        urlField.setText(remoteReportConfigDialogData.getUrl());
    }
}
