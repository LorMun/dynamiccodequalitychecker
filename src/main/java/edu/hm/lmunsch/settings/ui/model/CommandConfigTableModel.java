package edu.hm.lmunsch.settings.ui.model;

import com.intellij.openapi.project.Project;
import edu.hm.lmunsch.settings.entity.CommandConfigDialogData;
import edu.hm.lmunsch.settings.configProvider.ConfigProvider;
import edu.hm.lmunsch.settings.configProvider.ParserAndFileLocation;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CommandConfigTableModel extends AbstractTableModel {


    private List<CommandConfigDialogData> data = new ArrayList<>();
    private final String[] columnNames = {"Name", "Command", "Parser"};

    public CommandConfigTableModel(Project project) {
        final ConfigProvider configProvider = new ConfigProvider();
        ConfigProvider.ProjectSettings state = configProvider.getState();
        Map<String, CommandConfigDialogData> configuration = state.getConfiguration();
        if (configuration != null) {
            for (String key : configuration.keySet()) {
                data.add(configuration.get(key));
            }
        }
    }

    public void setData(final List<CommandConfigDialogData> data) {
        this.data = data;
    }

    public void addRow(CommandConfigDialogData rowData) {
        data.add(rowData);
        reloadTable();
    }

    public void reloadTable() {
        fireTableRowsInserted(data.size() - 1, data.size() - 1);

    }

    public int getColumnCount() {
        return columnNames.length;
    }

    public int getRowCount() {
        return data.size();
    }

    public List<CommandConfigDialogData> getData() {
        return data;
    }

    public String getColumnName(int col) {
        return columnNames[col];
    }

    public Object getValueAt(int row, int col) {
        CommandConfigDialogData rowElement = data.get(row);

        Object value = getValueForColumn(rowElement, col);
        return value;
    }

    public void removeConfigurationDataOnRow(int row) {
        data.remove(row);
        reloadTable();
    }

    public CommandConfigDialogData getConfigurationDataAtRow(int row) {
        return data.get(row);
    }

    private String getValueForColumn(CommandConfigDialogData data, int col) {
        switch (col) {
            case 0:
                return data.getName();
            case 1:
                return data.getCommand();
            case 2:
                return getAllParserNames(data);
            case 3:
                return getAllFileLocationNames(data);
            default:
                return "";
        }
    }

    private String getAllParserNames(CommandConfigDialogData commandConfigDialogData) {
        return commandConfigDialogData.getParserAndFileLocations().stream().map(ParserAndFileLocation::getParserName).collect(Collectors.joining(";  "));
    }

    private String getAllFileLocationNames(CommandConfigDialogData commandConfigDialogData) {
        return commandConfigDialogData.getParserAndFileLocations().stream().map(ParserAndFileLocation::getFilelocation).collect(Collectors.joining(";  "));
    }

    public Class getColumnClass(int c) {
        return getValueAt(0, c).getClass();
    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }

    public void setValueAt(Object value, int row, int col) {
        fireTableCellUpdated(row, col);
    }
}
