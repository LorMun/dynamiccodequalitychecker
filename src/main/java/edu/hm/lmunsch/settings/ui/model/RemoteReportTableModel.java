package edu.hm.lmunsch.settings.ui.model;

import com.intellij.openapi.project.Project;
import edu.hm.lmunsch.settings.configProvider.RemoteReportConfigProvider;
import edu.hm.lmunsch.settings.entity.RemoteReportConfigDialogData;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class RemoteReportTableModel extends AbstractTableModel {


    private List<RemoteReportConfigDialogData> data = new ArrayList<>();
    private final String[] columnNames = {"Name", "URL"};

    public RemoteReportTableModel(Project project) {
        final RemoteReportConfigProvider configProvider = new RemoteReportConfigProvider();
        RemoteReportConfigProvider.RemoteReportProjectSettings state = configProvider.getState();
        Map<String, RemoteReportConfigDialogData> configuration = state.getConfiguration();
        if (configuration != null) {
            for (String key : configuration.keySet()) {
                data.add(configuration.get(key));
            }
        }
    }

    public void setData(final List<RemoteReportConfigDialogData> data) {
        this.data = data;
    }

    public void addRow(RemoteReportConfigDialogData rowData) {
        data.add(rowData);
        reloadTable();
    }

    public void reloadTable() {
        fireTableRowsInserted(data.size() - 1, data.size() - 1);

    }

    public int getColumnCount() {
        return columnNames.length;
    }

    public int getRowCount() {
        return data.size();
    }

    public List<RemoteReportConfigDialogData> getData() {
        return data;
    }

    public String getColumnName(int col) {
        return columnNames[col];
    }

    public Object getValueAt(int row, int col) {
        RemoteReportConfigDialogData rowElement = data.get(row);

        Object value = getValueForColumn(rowElement, col);
        return value;
    }

    public void removeConfigurationDataOnRow(int row) {
        data.remove(row);
        reloadTable();
    }

    public RemoteReportConfigDialogData getConfigurationDataAtRow(int row) {
        return data.get(row);
    }

    private String getValueForColumn(RemoteReportConfigDialogData data, int col) {
        switch (col) {
            case 0:
                return data.getName();
            case 1:
                return data.getUrl();
            default:
                return "";
        }
    }

    public Class getColumnClass(int c) {
        return getValueAt(0, c).getClass();
    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }

    public void setValueAt(Object value, int row, int col) {
        fireTableCellUpdated(row, col);
    }
}
